/*
Navicat MySQL Data Transfer

Source Server         : khacdiep
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : buy_coin

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-03-27 12:35:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_account_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_account_admin`;
CREATE TABLE `tb_account_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_account_admin
-- ----------------------------
INSERT INTO `tb_account_admin` VALUES ('1', 'admin', '111111111');

-- ----------------------------
-- Table structure for tb_coin
-- ----------------------------
DROP TABLE IF EXISTS `tb_coin`;
CREATE TABLE `tb_coin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_coin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rate_buy` float NOT NULL DEFAULT '0',
  `rate_sell` float NOT NULL DEFAULT '0',
  `address_recive` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_buy` float NOT NULL DEFAULT '0',
  `max_buy` float NOT NULL DEFAULT '0',
  `min_sell` float NOT NULL DEFAULT '0',
  `max_sell` float NOT NULL DEFAULT '0',
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_coin
-- ----------------------------
INSERT INTO `tb_coin` VALUES ('1', 'bitcoin', '1', '/img/1520912425.png', 'Bitcoin', 'BTC', '21000', '22000', 'abcdef', '0.5', '20', '0.5', '20', '1111');
INSERT INTO `tb_coin` VALUES ('2', 'ethereum', '1', '/img/1520912055.png', 'Ethereum', 'ETH', '21500', '22500', 'abcdefgh', '0.1', '10', '0.3', '20', null);
INSERT INTO `tb_coin` VALUES ('3', 'bitcoin-cash', '1', '/img/1520912140.png', 'Bitcoin Cash', 'BCH', '19000', '20000', 'abcdef', '1', '1000', '2', '1000', null);
INSERT INTO `tb_coin` VALUES ('4', 'litecoin', '1', '/img/1520912338.png', 'Litecoin', 'LTC', '21000', '22000', 'abcdef', '10', '200', '10', '200', null);
INSERT INTO `tb_coin` VALUES ('5', 'dash', '1', '/img/1520912376.png', 'Dash', 'DASH', '24000', '26000', 'abcdef', '1', '300', '2', '250', null);
INSERT INTO `tb_coin` VALUES ('6', 'tether', '1', '/img/1520912409.png', 'Tether', 'USDT', '18000', '18500', 'abcdef', '0.05', '100', '0.01', '300', null);

-- ----------------------------
-- Table structure for tb_coin_show
-- ----------------------------
DROP TABLE IF EXISTS `tb_coin_show`;
CREATE TABLE `tb_coin_show` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_coin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_coin_show
-- ----------------------------
INSERT INTO `tb_coin_show` VALUES ('5', 'bitcoin');
INSERT INTO `tb_coin_show` VALUES ('6', 'tether');
INSERT INTO `tb_coin_show` VALUES ('7', 'ethereum');
INSERT INTO `tb_coin_show` VALUES ('8', 'litecoin');
INSERT INTO `tb_coin_show` VALUES ('9', 'dash');
INSERT INTO `tb_coin_show` VALUES ('10', 'bitcoin-cash');

-- ----------------------------
-- Table structure for tb_config
-- ----------------------------
DROP TABLE IF EXISTS `tb_config`;
CREATE TABLE `tb_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `notification` mediumtext COLLATE utf8_unicode_ci,
  `phone_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hour_start` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hour_start_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hour_end` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hour_end_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_transaction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_dieukhoan` text COLLATE utf8_unicode_ci,
  `timeout_load` int(11) NOT NULL DEFAULT '15',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_config
-- ----------------------------
INSERT INTO `tb_config` VALUES ('1', 'Hiện tại hệ thống VCB không ổn định nên các giao dịch sẽ được thanh toán trong thời gian 30 phút( thanh toán thủ công). Rất mong quý khách thông cảm.', '012365478899', '012369158623', 'abc@gmail.com', '09 Phan Kế Bính, Đakao, Q2', 'https://www.facebook.com/', '08:00', '13:00', '12:00', '18:00', '/public/upload/images/thunghiem2.png', '/upload/images/banner_slide.png', '/upload/images/banner3.png', 'VIETCOMBANK', 'Đồng nai', '1236547811236', 'Nguyễn Văn Anh', null, '<h2>Phần mềm d&agrave;nh cho ban quan l&yacute; dự &aacute;n</h2>\r\n\r\n<p>C&oacute; rất nhiều anh chị đ&atilde; đặt ra c&acirc;u hỏi: &quot;<strong><a href=\"http://vietapp.vn/tin-tuc/ban-quan-ly-du-an-nen-dung-phan-mem-gi.html\">Ban quản l&yacute; dự &aacute;n n&ecirc;n d&ugrave;ng phần mềm n&agrave;o</a>?</strong>&quot; hay &quot;<strong>Phần mềm d&agrave;nh cho ban quản l&yacute; dự &aacute;n l&agrave; g&igrave;?</strong>&quot;. Theo ch&uacute;ng t&ocirc;i những thắc mắc đ&oacute; l&agrave; ho&agrave;n to&agrave;n hợp l&yacute;. Vai tr&ograve; của ban quản l&yacute; dự &aacute;n rất quan trọng, do đ&oacute; họ cần được trang bị c&ocirc;ng cụ sản xuất ti&ecirc;n tiến. Nhằm mang lại hiệu quả đầu tư v&agrave; thuận tiện trong c&ocirc;ng t&aacute;c quản l&yacute;.</p>\r\n\r\n<p><img alt=\"Phần mềm dành cho ban quản lý dự án\" src=\"http://vietapp.vn/public/upload/images/phan-mem-cho-ban-quan-ly-du-an.jpg\" /></p>\r\n\r\n<p>Tr&ecirc;n cơ sở nắm bắt được c&aacute;c y&ecirc;u cầu nghiệp vụ của ban quản l&yacute; dự &aacute;n. VietApp đ&atilde; sản xuất phần mềm chuy&ecirc;n dụng cho ban quản l&yacute; dự &aacute;n. Phần mềm quản l&yacute; dự &aacute;n trực tuyến được trang bị hơn&nbsp;10 ph&acirc;n hệ chức năng ch&iacute;nh sau:</p>\r\n\r\n<h3><strong>Quản l&yacute; dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo dự &aacute;n mới</p>\r\n\r\n<p>Quản l&yacute; c&aacute;c th&ocirc;ng tin chi tiết của dự &aacute;n</p>\r\n\r\n<p>Theo d&otilde;i th&ocirc;ng tin tổng hợp v&agrave; cập nhật li&ecirc;n tục t&igrave;nh trạng của dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&acirc;n sự dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; chi tiết c&aacute;c th&ocirc;ng tin nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>G&aacute;n nh&acirc;n sự cho từng dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; g&oacute;i thầu, c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo, lập g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Tạo, lập kế hoạch đấu thầu</p>\r\n\r\n<p>Quản l&yacute; đấu thầu, lựa chọn nh&agrave; thầu</p>\r\n\r\n<p>Quản l&yacute; chi tiết th&ocirc;ng tin c&aacute;c g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&agrave; thầu dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&agrave; thầu dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết nh&agrave; thầu tham gia dự thầu, nh&agrave; thầu tr&uacute;ng thầu</p>\r\n\r\n<p>Quản l&yacute; năng lực nh&agrave; thầu: Nh&acirc;n sự, m&aacute;y m&oacute;c thiết bị, t&agrave;i ch&iacute;nh ...</p>\r\n\r\n<h3><strong>Quản l&yacute; hợp đồng</strong></h3>\r\n\r\n<p>Khởi tạo hợp đồng</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết hợp đồng</p>\r\n\r\n<p>Quản l&yacute; thanh to&aacute;n, quyết to&aacute;n hợp đồng</p>\r\n\r\n<h3><strong>Quản l&yacute; văn bản, t&agrave;i liệu dự &aacute;n</strong></h3>\r\n\r\n<p>Khởi tạo th&ocirc;ng tin t&agrave;i liệu</p>\r\n\r\n<p>Quản l&yacute; trực tuyến hệ thống văn bản, t&agrave;i liệu</p>\r\n\r\n<h3><strong>Xử l&yacute; văn bản đến</strong></h3>\r\n\r\n<h3><strong>Hệ thống giao việc trực tuyến</strong></h3>\r\n\r\n<p>Tạo giao việc theo chức năng, nhiệm vụ, g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Quản l&yacute;, theo d&otilde;i v&agrave; tương t&aacute;c giao việc mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>B&aacute;o c&aacute;o c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo b&aacute;o c&aacute;o tuần, qu&yacute;, năm của c&aacute; nh&acirc;n, ph&ograve;ng ban</p>\r\n\r\n<p>Quản l&yacute; to&agrave;n bộ b&aacute;o c&aacute;o của nh&acirc;n vi&ecirc;n, ph&ograve;ng ban mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>Quản l&yacute; tiến độ dự &aacute;n</strong></h3>\r\n\r\n<p>Lập tiến độ dự &aacute;n</p>\r\n\r\n<p>Cập nhật v&agrave; theo d&otilde;i tiến độ trực tuyến mọi l&uacute;c mọi nơi</p>\r\n\r\n<p>B&aacute;o c&aacute;o tiến độ v&agrave; xử l&yacute; sự cố bất kỳ thời điểm n&agrave;o</p>\r\n\r\n<h3>Xem chi tiết:&nbsp;<a href=\"http://vietapp.vn/san-pham/phan-mem-quan-ly-du-an-vietpm.html\"><strong>Phần mềm quản l&yacute; dự &aacute;n online</strong></a></h3>\r\n\r\n<p>Với việc trang bị đầy đủ c&aacute;c ph&acirc;n hệ như tr&ecirc;n, Ban quản l&yacute; dự &aacute;n ho&agrave;n to&agrave;n y&ecirc;n t&acirc;m về c&ocirc;ng &nbsp;cụ quản l&yacute; dự &aacute;n hiện đại VietPM n&agrave;y.</p>\r\n\r\n<p>H&atilde;y li&ecirc;n hệ ngay với ch&uacute;ng t&ocirc;i nếu bạn thực sự muốn quản l&yacute; &nbsp;dự &aacute;n hiệu quả. Đ&acirc;y l&agrave; giải ph&aacute;p quản trị dự &aacute;n mang tầm chiến lược của Doanh nghiệp, chủ đầu tư, ban quản l&yacute; dự &aacute;n. Ch&uacute;ng t&ocirc;i khẳng định rằng sự lựa chọn của bạn đối với VietPM&nbsp;sẽ mang lại cho bạn nhiều lơi &iacute;ch to lớn nhất.&nbsp;</p>', '15');

-- ----------------------------
-- Table structure for tb_help
-- ----------------------------
DROP TABLE IF EXISTS `tb_help`;
CREATE TABLE `tb_help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `summary` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_help
-- ----------------------------
INSERT INTO `tb_help` VALUES ('1', 'Tin tức 1 ádadad', '/img/1520347595.png', 'tin-tuc-1-adadad', '<p>dsdadadad</p>', '0', '0000-00-00 00:00:00', '');
INSERT INTO `tb_help` VALUES ('2', 'Phần mềm dành cho ban quản lý dự án', '/img/1520350335.png', 'phan-mem-danh-cho-ban-quan-ly-du-an', '<p>C&oacute; rất nhiều anh chị đ&atilde; đặt ra c&acirc;u hỏi: &quot;<strong><a href=\"http://vietapp.vn/tin-tuc/ban-quan-ly-du-an-nen-dung-phan-mem-gi.html\">Ban quản l&yacute; dự &aacute;n n&ecirc;n d&ugrave;ng phần mềm n&agrave;o</a>?</strong>&quot; hay &quot;<strong>Phần mềm d&agrave;nh cho ban quản l&yacute; dự &aacute;n l&agrave; g&igrave;?</strong>&quot;. Theo ch&uacute;ng t&ocirc;i những thắc mắc đ&oacute; l&agrave; ho&agrave;n to&agrave;n hợp l&yacute;. Vai tr&ograve; của ban quản l&yacute; dự &aacute;n rất quan trọng, do đ&oacute; họ cần được trang bị c&ocirc;ng cụ sản xuất ti&ecirc;n tiến. Nhằm mang lại hiệu quả đầu tư v&agrave; thuận tiện trong c&ocirc;ng t&aacute;c quản l&yacute;.</p>\r\n\r\n<p><img alt=\"Phần mềm dành cho ban quản lý dự án\" src=\"http://vietapp.vn/public/upload/images/phan-mem-cho-ban-quan-ly-du-an.jpg\" /></p>\r\n\r\n<p>Tr&ecirc;n cơ sở nắm bắt được c&aacute;c y&ecirc;u cầu nghiệp vụ của ban quản l&yacute; dự &aacute;n. VietApp đ&atilde; sản xuất phần mềm chuy&ecirc;n dụng cho ban quản l&yacute; dự &aacute;n. Phần mềm quản l&yacute; dự &aacute;n trực tuyến được trang bị hơn&nbsp;10 ph&acirc;n hệ chức năng ch&iacute;nh sau:</p>\r\n\r\n<h3><strong>Quản l&yacute; dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo dự &aacute;n mới</p>\r\n\r\n<p>Quản l&yacute; c&aacute;c th&ocirc;ng tin chi tiết của dự &aacute;n</p>\r\n\r\n<p>Theo d&otilde;i th&ocirc;ng tin tổng hợp v&agrave; cập nhật li&ecirc;n tục t&igrave;nh trạng của dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&acirc;n sự dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; chi tiết c&aacute;c th&ocirc;ng tin nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>G&aacute;n nh&acirc;n sự cho từng dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; g&oacute;i thầu, c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo, lập g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Tạo, lập kế hoạch đấu thầu</p>\r\n\r\n<p>Quản l&yacute; đấu thầu, lựa chọn nh&agrave; thầu</p>\r\n\r\n<p>Quản l&yacute; chi tiết th&ocirc;ng tin c&aacute;c g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&agrave; thầu dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&agrave; thầu dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết nh&agrave; thầu tham gia dự thầu, nh&agrave; thầu tr&uacute;ng thầu</p>\r\n\r\n<p>Quản l&yacute; năng lực nh&agrave; thầu: Nh&acirc;n sự, m&aacute;y m&oacute;c thiết bị, t&agrave;i ch&iacute;nh ...</p>\r\n\r\n<h3><strong>Quản l&yacute; hợp đồng</strong></h3>\r\n\r\n<p>Khởi tạo hợp đồng</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết hợp đồng</p>\r\n\r\n<p>Quản l&yacute; thanh to&aacute;n, quyết to&aacute;n hợp đồng</p>\r\n\r\n<h3><strong>Quản l&yacute; văn bản, t&agrave;i liệu dự &aacute;n</strong></h3>\r\n\r\n<p>Khởi tạo th&ocirc;ng tin t&agrave;i liệu</p>\r\n\r\n<p>Quản l&yacute; trực tuyến hệ thống văn bản, t&agrave;i liệu</p>\r\n\r\n<h3><strong>Xử l&yacute; văn bản đến</strong></h3>\r\n\r\n<h3><strong>Hệ thống giao việc trực tuyến</strong></h3>\r\n\r\n<p>Tạo giao việc theo chức năng, nhiệm vụ, g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Quản l&yacute;, theo d&otilde;i v&agrave; tương t&aacute;c giao việc mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>B&aacute;o c&aacute;o c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo b&aacute;o c&aacute;o tuần, qu&yacute;, năm của c&aacute; nh&acirc;n, ph&ograve;ng ban</p>\r\n\r\n<p>Quản l&yacute; to&agrave;n bộ b&aacute;o c&aacute;o của nh&acirc;n vi&ecirc;n, ph&ograve;ng ban mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>Quản l&yacute; tiến độ dự &aacute;n</strong></h3>\r\n\r\n<p>Lập tiến độ dự &aacute;n</p>\r\n\r\n<p>Cập nhật v&agrave; theo d&otilde;i tiến độ trực tuyến mọi l&uacute;c mọi nơi</p>\r\n\r\n<p>B&aacute;o c&aacute;o tiến độ v&agrave; xử l&yacute; sự cố bất kỳ thời điểm n&agrave;o</p>\r\n\r\n<h3>Xem chi tiết:&nbsp;<a href=\"http://vietapp.vn/san-pham/phan-mem-quan-ly-du-an-vietpm.html\"><strong>Phần mềm quản l&yacute; dự &aacute;n online</strong></a></h3>\r\n\r\n<p>Với việc trang bị đầy đủ c&aacute;c ph&acirc;n hệ như tr&ecirc;n, Ban quản l&yacute; dự &aacute;n ho&agrave;n to&agrave;n y&ecirc;n t&acirc;m về c&ocirc;ng &nbsp;cụ quản l&yacute; dự &aacute;n hiện đại VietPM n&agrave;y.</p>\r\n\r\n<p>H&atilde;y li&ecirc;n hệ ngay với ch&uacute;ng t&ocirc;i nếu bạn thực sự muốn quản l&yacute; &nbsp;dự &aacute;n hiệu quả. Đ&acirc;y l&agrave; giải ph&aacute;p quản trị dự &aacute;n mang tầm chiến lược của Doanh nghiệp, chủ đầu tư, ban quản l&yacute; dự &aacute;n. Ch&uacute;ng t&ocirc;i khẳng định rằng sự lựa chọn của bạn đối với VietPM&nbsp;sẽ mang lại cho bạn nhiều lơi &iacute;ch to lớn nhất.&nbsp;</p>', '1', '2018-03-06 22:32:15', 'Có rất nhiều anh chị đã đặt ra câu hỏi: \"Ban quản lý dự án nên dùng phần mềm nào?\" hay \"Phần mềm dành cho ban quản lý dự án là gì?\". Theo chúng tôi những thắc mắc đó là hoàn toàn hợp lý. Vai trò của ban quản lý dự án rất quan trọng, do đó họ cần được trang bị công cụ sản xuất tiên tiến. Nhằm mang lại hiệu quả đầu tư và thuận tiện trong công tác quản lý.');
INSERT INTO `tb_help` VALUES ('3', 'Phần mềm không thể tốt hơn cho ban quản lý dự án', '/img/1520351977.png', 'phan-mem-khong-the-tot-hon-cho-ban-quan-ly-du-an', '<h2>Phần mềm d&agrave;nh cho ban quan l&yacute; dự &aacute;n</h2>\r\n\r\n<p>C&oacute; rất nhiều anh chị đ&atilde; đặt ra c&acirc;u hỏi: &quot;<strong><a href=\"http://vietapp.vn/tin-tuc/ban-quan-ly-du-an-nen-dung-phan-mem-gi.html\">Ban quản l&yacute; dự &aacute;n n&ecirc;n d&ugrave;ng phần mềm n&agrave;o</a>?</strong>&quot; hay &quot;<strong>Phần mềm d&agrave;nh cho ban quản l&yacute; dự &aacute;n l&agrave; g&igrave;?</strong>&quot;. Theo ch&uacute;ng t&ocirc;i những thắc mắc đ&oacute; l&agrave; ho&agrave;n to&agrave;n hợp l&yacute;. Vai tr&ograve; của ban quản l&yacute; dự &aacute;n rất quan trọng, do đ&oacute; họ cần được trang bị c&ocirc;ng cụ sản xuất ti&ecirc;n tiến. Nhằm mang lại hiệu quả đầu tư v&agrave; thuận tiện trong c&ocirc;ng t&aacute;c quản l&yacute;.</p>\r\n\r\n<p><img alt=\"Phần mềm dành cho ban quản lý dự án\" src=\"http://vietapp.vn/public/upload/images/phan-mem-cho-ban-quan-ly-du-an.jpg\" /></p>\r\n\r\n<p>Tr&ecirc;n cơ sở nắm bắt được c&aacute;c y&ecirc;u cầu nghiệp vụ của ban quản l&yacute; dự &aacute;n. VietApp đ&atilde; sản xuất phần mềm chuy&ecirc;n dụng cho ban quản l&yacute; dự &aacute;n. Phần mềm quản l&yacute; dự &aacute;n trực tuyến được trang bị hơn&nbsp;10 ph&acirc;n hệ chức năng ch&iacute;nh sau:</p>\r\n\r\n<h3><strong>Quản l&yacute; dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo dự &aacute;n mới</p>\r\n\r\n<p>Quản l&yacute; c&aacute;c th&ocirc;ng tin chi tiết của dự &aacute;n</p>\r\n\r\n<p>Theo d&otilde;i th&ocirc;ng tin tổng hợp v&agrave; cập nhật li&ecirc;n tục t&igrave;nh trạng của dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&acirc;n sự dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; chi tiết c&aacute;c th&ocirc;ng tin nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>G&aacute;n nh&acirc;n sự cho từng dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; g&oacute;i thầu, c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo, lập g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Tạo, lập kế hoạch đấu thầu</p>\r\n\r\n<p>Quản l&yacute; đấu thầu, lựa chọn nh&agrave; thầu</p>\r\n\r\n<p>Quản l&yacute; chi tiết th&ocirc;ng tin c&aacute;c g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&agrave; thầu dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&agrave; thầu dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết nh&agrave; thầu tham gia dự thầu, nh&agrave; thầu tr&uacute;ng thầu</p>\r\n\r\n<p>Quản l&yacute; năng lực nh&agrave; thầu: Nh&acirc;n sự, m&aacute;y m&oacute;c thiết bị, t&agrave;i ch&iacute;nh ...</p>\r\n\r\n<h3><strong>Quản l&yacute; hợp đồng</strong></h3>\r\n\r\n<p>Khởi tạo hợp đồng</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết hợp đồng</p>\r\n\r\n<p>Quản l&yacute; thanh to&aacute;n, quyết to&aacute;n hợp đồng</p>\r\n\r\n<h3><strong>Quản l&yacute; văn bản, t&agrave;i liệu dự &aacute;n</strong></h3>\r\n\r\n<p>Khởi tạo th&ocirc;ng tin t&agrave;i liệu</p>\r\n\r\n<p>Quản l&yacute; trực tuyến hệ thống văn bản, t&agrave;i liệu</p>\r\n\r\n<h3><strong>Xử l&yacute; văn bản đến</strong></h3>\r\n\r\n<h3><strong>Hệ thống giao việc trực tuyến</strong></h3>\r\n\r\n<p>Tạo giao việc theo chức năng, nhiệm vụ, g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Quản l&yacute;, theo d&otilde;i v&agrave; tương t&aacute;c giao việc mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>B&aacute;o c&aacute;o c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo b&aacute;o c&aacute;o tuần, qu&yacute;, năm của c&aacute; nh&acirc;n, ph&ograve;ng ban</p>\r\n\r\n<p>Quản l&yacute; to&agrave;n bộ b&aacute;o c&aacute;o của nh&acirc;n vi&ecirc;n, ph&ograve;ng ban mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>Quản l&yacute; tiến độ dự &aacute;n</strong></h3>\r\n\r\n<p>Lập tiến độ dự &aacute;n</p>\r\n\r\n<p>Cập nhật v&agrave; theo d&otilde;i tiến độ trực tuyến mọi l&uacute;c mọi nơi</p>\r\n\r\n<p>B&aacute;o c&aacute;o tiến độ v&agrave; xử l&yacute; sự cố bất kỳ thời điểm n&agrave;o</p>\r\n\r\n<h3>Xem chi tiết:&nbsp;<a href=\"http://vietapp.vn/san-pham/phan-mem-quan-ly-du-an-vietpm.html\"><strong>Phần mềm quản l&yacute; dự &aacute;n online</strong></a></h3>\r\n\r\n<p>Với việc trang bị đầy đủ c&aacute;c ph&acirc;n hệ như tr&ecirc;n, Ban quản l&yacute; dự &aacute;n ho&agrave;n to&agrave;n y&ecirc;n t&acirc;m về c&ocirc;ng &nbsp;cụ quản l&yacute; dự &aacute;n hiện đại VietPM n&agrave;y.</p>\r\n\r\n<p>H&atilde;y li&ecirc;n hệ ngay với ch&uacute;ng t&ocirc;i nếu bạn thực sự muốn quản l&yacute; &nbsp;dự &aacute;n hiệu quả. Đ&acirc;y l&agrave; giải ph&aacute;p quản trị dự &aacute;n mang tầm chiến lược của Doanh nghiệp, chủ đầu tư, ban quản l&yacute; dự &aacute;n. Ch&uacute;ng t&ocirc;i khẳng định rằng sự lựa chọn của bạn đối với VietPM&nbsp;sẽ mang lại cho bạn nhiều lơi &iacute;ch to lớn nhất.&nbsp;</p>', '1', '2018-03-06 22:59:37', 'Có rất nhiều anh chị đã đặt ra câu hỏi: \"Ban quản lý dự án nên dùng phần mềm nào?\" hay \"Phần mềm dành cho ban quản lý dự án là gì?\". Theo chúng tôi những thắc mắc đó là hoàn toàn hợp lý. Vai trò của ban quản lý dự án rất quan trọng, do đó họ cần được trang bị công cụ sản xuất tiên tiến. Nhằm mang lại hiệu quả đầu tư và thuận tiện trong công tác quản lý.');

-- ----------------------------
-- Table structure for tb_img_coin
-- ----------------------------
DROP TABLE IF EXISTS `tb_img_coin`;
CREATE TABLE `tb_img_coin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_coin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_img_coin
-- ----------------------------
INSERT INTO `tb_img_coin` VALUES ('2', 'bitcoin', '/img/1520403877.png');
INSERT INTO `tb_img_coin` VALUES ('3', 'tether', '/img/1520404630.png');
INSERT INTO `tb_img_coin` VALUES ('4', 'ethereum', '/img/1520404687.png');
INSERT INTO `tb_img_coin` VALUES ('5', 'litecoin', '/img/1520404708.png');
INSERT INTO `tb_img_coin` VALUES ('6', 'dash', '/img/1520404744.png');
INSERT INTO `tb_img_coin` VALUES ('7', 'bitcoin-cash', '/img/1520404760.png');

-- ----------------------------
-- Table structure for tb_news
-- ----------------------------
DROP TABLE IF EXISTS `tb_news`;
CREATE TABLE `tb_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_news
-- ----------------------------
INSERT INTO `tb_news` VALUES ('2', 'Phần mềm dành cho ban quản lý dự án', '/img/1520350335.png', 'phan-mem-danh-cho-ban-quan-ly-du-an.html', 'Có rất nhiều anh chị đã đặt ra câu hỏi: \"Ban quản lý dự án nên dùng phần mềm nào?\" hay \"Phần mềm dành cho ban quản lý dự án là gì?\". Theo chúng tôi những thắc mắc đó là hoàn toàn hợp lý. Vai trò của ban quản lý dự án rất quan trọng, do đó họ cần được trang bị công cụ sản xuất tiên tiến. Nhằm mang lại hiệu quả đầu tư và thuận tiện trong công tác quản lý.', '<p>C&oacute; rất nhiều anh chị đ&atilde; đặt ra c&acirc;u hỏi: &quot;<strong><a href=\"http://vietapp.vn/tin-tuc/ban-quan-ly-du-an-nen-dung-phan-mem-gi.html\">Ban quản l&yacute; dự &aacute;n n&ecirc;n d&ugrave;ng phần mềm n&agrave;o</a>?</strong>&quot; hay &quot;<strong>Phần mềm d&agrave;nh cho ban quản l&yacute; dự &aacute;n l&agrave; g&igrave;?</strong>&quot;. Theo ch&uacute;ng t&ocirc;i những thắc mắc đ&oacute; l&agrave; ho&agrave;n to&agrave;n hợp l&yacute;. Vai tr&ograve; của ban quản l&yacute; dự &aacute;n rất quan trọng, do đ&oacute; họ cần được trang bị c&ocirc;ng cụ sản xuất ti&ecirc;n tiến. Nhằm mang lại hiệu quả đầu tư v&agrave; thuận tiện trong c&ocirc;ng t&aacute;c quản l&yacute;.</p>\r\n\r\n<p><img alt=\"Phần mềm dành cho ban quản lý dự án\" src=\"http://vietapp.vn/public/upload/images/phan-mem-cho-ban-quan-ly-du-an.jpg\" /></p>\r\n\r\n<p>Tr&ecirc;n cơ sở nắm bắt được c&aacute;c y&ecirc;u cầu nghiệp vụ của ban quản l&yacute; dự &aacute;n. VietApp đ&atilde; sản xuất phần mềm chuy&ecirc;n dụng cho ban quản l&yacute; dự &aacute;n. Phần mềm quản l&yacute; dự &aacute;n trực tuyến được trang bị hơn&nbsp;10 ph&acirc;n hệ chức năng ch&iacute;nh sau:</p>\r\n\r\n<h3><strong>Quản l&yacute; dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo dự &aacute;n mới</p>\r\n\r\n<p>Quản l&yacute; c&aacute;c th&ocirc;ng tin chi tiết của dự &aacute;n</p>\r\n\r\n<p>Theo d&otilde;i th&ocirc;ng tin tổng hợp v&agrave; cập nhật li&ecirc;n tục t&igrave;nh trạng của dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&acirc;n sự dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; chi tiết c&aacute;c th&ocirc;ng tin nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>G&aacute;n nh&acirc;n sự cho từng dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; g&oacute;i thầu, c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo, lập g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Tạo, lập kế hoạch đấu thầu</p>\r\n\r\n<p>Quản l&yacute; đấu thầu, lựa chọn nh&agrave; thầu</p>\r\n\r\n<p>Quản l&yacute; chi tiết th&ocirc;ng tin c&aacute;c g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&agrave; thầu dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&agrave; thầu dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết nh&agrave; thầu tham gia dự thầu, nh&agrave; thầu tr&uacute;ng thầu</p>\r\n\r\n<p>Quản l&yacute; năng lực nh&agrave; thầu: Nh&acirc;n sự, m&aacute;y m&oacute;c thiết bị, t&agrave;i ch&iacute;nh ...</p>\r\n\r\n<h3><strong>Quản l&yacute; hợp đồng</strong></h3>\r\n\r\n<p>Khởi tạo hợp đồng</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết hợp đồng</p>\r\n\r\n<p>Quản l&yacute; thanh to&aacute;n, quyết to&aacute;n hợp đồng</p>\r\n\r\n<h3><strong>Quản l&yacute; văn bản, t&agrave;i liệu dự &aacute;n</strong></h3>\r\n\r\n<p>Khởi tạo th&ocirc;ng tin t&agrave;i liệu</p>\r\n\r\n<p>Quản l&yacute; trực tuyến hệ thống văn bản, t&agrave;i liệu</p>\r\n\r\n<h3><strong>Xử l&yacute; văn bản đến</strong></h3>\r\n\r\n<h3><strong>Hệ thống giao việc trực tuyến</strong></h3>\r\n\r\n<p>Tạo giao việc theo chức năng, nhiệm vụ, g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Quản l&yacute;, theo d&otilde;i v&agrave; tương t&aacute;c giao việc mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>B&aacute;o c&aacute;o c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo b&aacute;o c&aacute;o tuần, qu&yacute;, năm của c&aacute; nh&acirc;n, ph&ograve;ng ban</p>\r\n\r\n<p>Quản l&yacute; to&agrave;n bộ b&aacute;o c&aacute;o của nh&acirc;n vi&ecirc;n, ph&ograve;ng ban mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>Quản l&yacute; tiến độ dự &aacute;n</strong></h3>\r\n\r\n<p>Lập tiến độ dự &aacute;n</p>\r\n\r\n<p>Cập nhật v&agrave; theo d&otilde;i tiến độ trực tuyến mọi l&uacute;c mọi nơi</p>\r\n\r\n<p>B&aacute;o c&aacute;o tiến độ v&agrave; xử l&yacute; sự cố bất kỳ thời điểm n&agrave;o</p>\r\n\r\n<h3>Xem chi tiết:&nbsp;<a href=\"http://vietapp.vn/san-pham/phan-mem-quan-ly-du-an-vietpm.html\"><strong>Phần mềm quản l&yacute; dự &aacute;n online</strong></a></h3>\r\n\r\n<p>Với việc trang bị đầy đủ c&aacute;c ph&acirc;n hệ như tr&ecirc;n, Ban quản l&yacute; dự &aacute;n ho&agrave;n to&agrave;n y&ecirc;n t&acirc;m về c&ocirc;ng &nbsp;cụ quản l&yacute; dự &aacute;n hiện đại VietPM n&agrave;y.</p>\r\n\r\n<p>H&atilde;y li&ecirc;n hệ ngay với ch&uacute;ng t&ocirc;i nếu bạn thực sự muốn quản l&yacute; &nbsp;dự &aacute;n hiệu quả. Đ&acirc;y l&agrave; giải ph&aacute;p quản trị dự &aacute;n mang tầm chiến lược của Doanh nghiệp, chủ đầu tư, ban quản l&yacute; dự &aacute;n. Ch&uacute;ng t&ocirc;i khẳng định rằng sự lựa chọn của bạn đối với VietPM&nbsp;sẽ mang lại cho bạn nhiều lơi &iacute;ch to lớn nhất.&nbsp;</p>', 'Ban quản lý dự án#Quản lý dự án đầu tư#', 'ban-quan-ly-du-an#quan-ly-du-an-dau-tu#', '1', '2018-03-06 22:32:15');
INSERT INTO `tb_news` VALUES ('3', 'Phần mềm không thể tốt hơn cho ban quản lý dự án', '/img/1520351977.png', 'phan-mem-khong-the-tot-hon-cho-ban-quan-ly-du-an', 'Có rất nhiều anh chị đã đặt ra câu hỏi: \"Ban quản lý dự án nên dùng phần mềm nào?\" hay \"Phần mềm dành cho ban quản lý dự án là gì?\"', '<h2>Phần mềm d&agrave;nh cho ban quan l&yacute; dự &aacute;n</h2>\r\n\r\n<p>C&oacute; rất nhiều anh chị đ&atilde; đặt ra c&acirc;u hỏi: &quot;<strong><a href=\"http://vietapp.vn/tin-tuc/ban-quan-ly-du-an-nen-dung-phan-mem-gi.html\">Ban quản l&yacute; dự &aacute;n n&ecirc;n d&ugrave;ng phần mềm n&agrave;o</a>?</strong>&quot; hay &quot;<strong>Phần mềm d&agrave;nh cho ban quản l&yacute; dự &aacute;n l&agrave; g&igrave;?</strong>&quot;. Theo ch&uacute;ng t&ocirc;i những thắc mắc đ&oacute; l&agrave; ho&agrave;n to&agrave;n hợp l&yacute;. Vai tr&ograve; của ban quản l&yacute; dự &aacute;n rất quan trọng, do đ&oacute; họ cần được trang bị c&ocirc;ng cụ sản xuất ti&ecirc;n tiến. Nhằm mang lại hiệu quả đầu tư v&agrave; thuận tiện trong c&ocirc;ng t&aacute;c quản l&yacute;.</p>\r\n\r\n<p><img alt=\"Phần mềm dành cho ban quản lý dự án\" src=\"http://vietapp.vn/public/upload/images/phan-mem-cho-ban-quan-ly-du-an.jpg\" /></p>\r\n\r\n<p>Tr&ecirc;n cơ sở nắm bắt được c&aacute;c y&ecirc;u cầu nghiệp vụ của ban quản l&yacute; dự &aacute;n. VietApp đ&atilde; sản xuất phần mềm chuy&ecirc;n dụng cho ban quản l&yacute; dự &aacute;n. Phần mềm quản l&yacute; dự &aacute;n trực tuyến được trang bị hơn&nbsp;10 ph&acirc;n hệ chức năng ch&iacute;nh sau:</p>\r\n\r\n<h3><strong>Quản l&yacute; dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo dự &aacute;n mới</p>\r\n\r\n<p>Quản l&yacute; c&aacute;c th&ocirc;ng tin chi tiết của dự &aacute;n</p>\r\n\r\n<p>Theo d&otilde;i th&ocirc;ng tin tổng hợp v&agrave; cập nhật li&ecirc;n tục t&igrave;nh trạng của dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&acirc;n sự dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; chi tiết c&aacute;c th&ocirc;ng tin nh&acirc;n sự dự &aacute;n</p>\r\n\r\n<p>G&aacute;n nh&acirc;n sự cho từng dự &aacute;n</p>\r\n\r\n<h3><strong>Quản l&yacute; g&oacute;i thầu, c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo, lập g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Tạo, lập kế hoạch đấu thầu</p>\r\n\r\n<p>Quản l&yacute; đấu thầu, lựa chọn nh&agrave; thầu</p>\r\n\r\n<p>Quản l&yacute; chi tiết th&ocirc;ng tin c&aacute;c g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<h3><strong>Quản l&yacute; nh&agrave; thầu dự &aacute;n</strong></h3>\r\n\r\n<p>Tạo nh&agrave; thầu dự &aacute;n</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết nh&agrave; thầu tham gia dự thầu, nh&agrave; thầu tr&uacute;ng thầu</p>\r\n\r\n<p>Quản l&yacute; năng lực nh&agrave; thầu: Nh&acirc;n sự, m&aacute;y m&oacute;c thiết bị, t&agrave;i ch&iacute;nh ...</p>\r\n\r\n<h3><strong>Quản l&yacute; hợp đồng</strong></h3>\r\n\r\n<p>Khởi tạo hợp đồng</p>\r\n\r\n<p>Quản l&yacute; th&ocirc;ng tin chi tiết hợp đồng</p>\r\n\r\n<p>Quản l&yacute; thanh to&aacute;n, quyết to&aacute;n hợp đồng</p>\r\n\r\n<h3><strong>Quản l&yacute; văn bản, t&agrave;i liệu dự &aacute;n</strong></h3>\r\n\r\n<p>Khởi tạo th&ocirc;ng tin t&agrave;i liệu</p>\r\n\r\n<p>Quản l&yacute; trực tuyến hệ thống văn bản, t&agrave;i liệu</p>\r\n\r\n<h3><strong>Xử l&yacute; văn bản đến</strong></h3>\r\n\r\n<h3><strong>Hệ thống giao việc trực tuyến</strong></h3>\r\n\r\n<p>Tạo giao việc theo chức năng, nhiệm vụ, g&oacute;i thầu, c&ocirc;ng việc</p>\r\n\r\n<p>Quản l&yacute;, theo d&otilde;i v&agrave; tương t&aacute;c giao việc mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>B&aacute;o c&aacute;o c&ocirc;ng việc</strong></h3>\r\n\r\n<p>Tạo b&aacute;o c&aacute;o tuần, qu&yacute;, năm của c&aacute; nh&acirc;n, ph&ograve;ng ban</p>\r\n\r\n<p>Quản l&yacute; to&agrave;n bộ b&aacute;o c&aacute;o của nh&acirc;n vi&ecirc;n, ph&ograve;ng ban mọi l&uacute;c mọi nơi</p>\r\n\r\n<h3><strong>Quản l&yacute; tiến độ dự &aacute;n</strong></h3>\r\n\r\n<p>Lập tiến độ dự &aacute;n</p>\r\n\r\n<p>Cập nhật v&agrave; theo d&otilde;i tiến độ trực tuyến mọi l&uacute;c mọi nơi</p>\r\n\r\n<p>B&aacute;o c&aacute;o tiến độ v&agrave; xử l&yacute; sự cố bất kỳ thời điểm n&agrave;o</p>\r\n\r\n<h3>Xem chi tiết:&nbsp;<a href=\"http://vietapp.vn/san-pham/phan-mem-quan-ly-du-an-vietpm.html\"><strong>Phần mềm quản l&yacute; dự &aacute;n online</strong></a></h3>\r\n\r\n<p>Với việc trang bị đầy đủ c&aacute;c ph&acirc;n hệ như tr&ecirc;n, Ban quản l&yacute; dự &aacute;n ho&agrave;n to&agrave;n y&ecirc;n t&acirc;m về c&ocirc;ng &nbsp;cụ quản l&yacute; dự &aacute;n hiện đại VietPM n&agrave;y.</p>\r\n\r\n<p>H&atilde;y li&ecirc;n hệ ngay với ch&uacute;ng t&ocirc;i nếu bạn thực sự muốn quản l&yacute; &nbsp;dự &aacute;n hiệu quả. Đ&acirc;y l&agrave; giải ph&aacute;p quản trị dự &aacute;n mang tầm chiến lược của Doanh nghiệp, chủ đầu tư, ban quản l&yacute; dự &aacute;n. Ch&uacute;ng t&ocirc;i khẳng định rằng sự lựa chọn của bạn đối với VietPM&nbsp;sẽ mang lại cho bạn nhiều lơi &iacute;ch to lớn nhất.&nbsp;</p>', null, null, '1', '2018-03-06 22:59:37');

-- ----------------------------
-- Table structure for tb_report
-- ----------------------------
DROP TABLE IF EXISTS `tb_report`;
CREATE TABLE `tb_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_report
-- ----------------------------
INSERT INTO `tb_report` VALUES ('1', '090.xxxx.220', 'Rất nhanh nhé', '2018-03-08 10:35:30');
INSERT INTO `tb_report` VALUES ('3', '098.xxxx.269', 'Giao dịch eth qua Vietinbank rất nhanh chưa đến 5p đã nhận tiền đc', '2018-03-08 10:47:05');

-- ----------------------------
-- Table structure for tb_sell_buy
-- ----------------------------
DROP TABLE IF EXISTS `tb_sell_buy`;
CREATE TABLE `tb_sell_buy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `number_coin` float NOT NULL DEFAULT '0',
  `money` float NOT NULL DEFAULT '0',
  `account_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` int(4) NOT NULL DEFAULT '0',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_coin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_action` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: sell 1:buy',
  `address_coin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_pay` tinyint(4) NOT NULL DEFAULT '0',
  `code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_sell_buy
-- ----------------------------
INSERT INTO `tb_sell_buy` VALUES ('17', '5', '1', '21000000', null, '1', '2018-03-15 13:11:34', 'bitcoin', '1', 'DKEOSKDOS', null, '1', 'aksidoemg8');
INSERT INTO `tb_sell_buy` VALUES ('18', '5', '2', '10000000', '2365142547956', '1', '2018-03-15 13:47:58', 'dash', '0', 'TOSLDKEMDK', 'adsd', '1', '3odlsmdkai');
INSERT INTO `tb_sell_buy` VALUES ('19', '5', '0.5', '10500', null, '1', '2018-03-16 10:50:09', 'bitcoin', '1', 'FWQEWQEWQ', null, '1', 'vmskdloair');
INSERT INTO `tb_sell_buy` VALUES ('20', '5', '20', '0', '12365478965412', '2', '2018-03-16 11:00:29', 'litecoin', '0', 'SADADASD', null, '0', 'amskdlobkd');
INSERT INTO `tb_sell_buy` VALUES ('21', '5', '1', '0', '1111111111111', '0', '2018-03-19 22:39:22', 'bitcoin', '0', 'lsodpa', null, '0', 'EwTRAh0BYm');
INSERT INTO `tb_sell_buy` VALUES ('22', '5', '1', '179847000', null, '0', '2018-03-19 22:40:16', 'bitcoin', '1', 'pelsda', 'abc, des', '0', 'GzotfhWRyE');
INSERT INTO `tb_sell_buy` VALUES ('23', '5', '0.8', '153311000', null, '0', '2018-03-21 11:54:00', 'bitcoin', '1', 'skdoal', 'abc', '0', 'OQ8aYf21nR');
INSERT INTO `tb_sell_buy` VALUES ('24', '5', '0.5', '95902600', null, '2', '2018-03-21 12:06:09', 'bitcoin', '1', 'sewasd', null, '0', '9mm7AoMZWR');
INSERT INTO `tb_sell_buy` VALUES ('25', '5', '1', '0', '1236514521365', '2', '2018-03-21 12:30:19', 'bitcoin', '0', 'qsdasd', 'ưec', '0', 'h8g1vP8MSM');
INSERT INTO `tb_sell_buy` VALUES ('26', '5', '3', '532027000', null, '2', '2018-03-23 10:00:05', 'bitcoin', '1', 'plwosd', null, '0', 'eTFuyOUaWD');
INSERT INTO `tb_sell_buy` VALUES ('28', '5', '1', '0', '1365142123651', '0', '2018-03-23 14:32:22', 'bitcoin', '0', 'asdfaf', null, '0', 'zz412uH5fx');
INSERT INTO `tb_sell_buy` VALUES ('29', '5', '1', '0', '1111111111111', '2', '2018-03-23 14:42:30', 'bitcoin', '0', 'sdasda', null, '0', 'aALshiDAx8');
INSERT INTO `tb_sell_buy` VALUES ('30', '5', '12', '40226800', null, '2', '2018-03-25 13:08:01', 'litecoin', '1', '111111', null, '0', 'hGVzPcUByW');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` tinyint(2) NOT NULL DEFAULT '1',
  `date_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'vananh@gmail.com', '123456789', 'Nguyễn Văn Anh', '0123651421', '1', '2018-03-08 10:32:49', null, null);
INSERT INTO `tb_user` VALUES ('2', 'quangha@gmail.com', '2156481321', 'Phạm Quang Hà', '01656511', '1', '2018-03-08 10:32:49', null, null);
INSERT INTO `tb_user` VALUES ('3', 'dangkhoi@gmail.com', '2121212', 'Nguyễn Đăng Khôi', '01365214', '1', '2018-03-08 10:32:49', null, null);
INSERT INTO `tb_user` VALUES ('5', 'dieptk95@gmail.com', '$2y$10$1y6fmlC4fY/7LJT/nPLp2.894214yVoIpTtN7A6dugSUGekFZq6tS', 'Thái Khắc Điệp', '01636514240', '1', '2018-03-09 13:06:56', 'NA', 'fUpiQy4LPT7pgwDkTNkwAU8NmPyAhMZcLZjlSGIX5qXy8hIHhy4upxGPm5eT');
INSERT INTO `tb_user` VALUES ('7', 'thaikhacdiep@gmail.com', '$2y$10$JflijktWyAvzaJGiw4OX/.YiTkPRcImc67DQAkbBfxXSbEbgaUCjm', 'K Điệp', '123651264', '1', '2018-03-09 13:21:31', null, null);
