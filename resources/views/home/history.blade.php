<?php $dataH = app('App\Http\Controllers\HomeController')->getHistoryTransaction(); ?>
<div class="rateTable">
	<div class="panel panel-default mHistory">
		<div class="panel-heading"><i class="fa fa-bar-chart" aria-hidden="true"></i> Lịch Sử Giao Dịch</div>
		<div class="panel-body">
			<table class="table tb-history">
				<thead>
					<tr>
						<th class="center w_40"> Loại</th>
						<th class="w_60">ALT coin</th>
						<th class="center">Số lượng</th>
						<th class="right">Số tiền</th>
						<th class="center">Thời gian</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($dataH as $p)
						<tr>
							<td class="center">
								@if (!$p->type_action)
									Bán
								@else 
									Mua
								@endif
							</td>
							<td>
								{{$p->symbol}}
							</td>
							<td class="center">
								{{$p->number_coin}}
							</td>
							<td class="right">
								{{ number_format($p->money, 0)}}
							</td>
							<td class="center">
								{{ Carbon\Carbon::parse($p->time)->format('d/m/Y H:i:s') }}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>