@extends('home.master')

@section('title','Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC')

@section('content_buy_sell')
    <div id="snipper" class="col-sm-12">
			<div class="panel panel-primary">
			<div class="panel-heading">
					<div class="title_container">Đăng ký</div>
				</div>
				<div class="panel-body">
					<form action="/post-register-user" method="post" class="form-horizontal" >
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						@if (session()->has('errors'))
							<div id="error" style="color: red">{!! session()->get('errors') !!}</div>
						@endif
						<div class="form-group">
							<label class="col-lg-3 control-label">Email của bạn</label>
							<div class="col-lg-9">
								<input type="email" name="email" class="form-control" placeholder="Email của bạn" required autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Mật khẩu</label>
							<div class="col-lg-9">
								<input type="password" name="password" class="form-control" placeholder="Mật khẩu của bạn (ít nhất 8 ký tự)" required autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Nhập lại</label>
							<div class="col-lg-9">
								<input type="password" name="repassword" class="form-control" placeholder="Nhập lại mật khẩu" required autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Họ và Tên</label>
							<div class="col-lg-9">
								<input type="text" name="fullname" class="form-control" placeholder="Họ tên của bạn" required autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Số điện thoại</label>
							<div class="col-lg-9">
								<input type="tel" name="phone" class="form-control" placeholder="Số điện thoại của bạn" required autocomplete="off">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-5 col-md-offset-5">
								<button class="btn btn-primary uptext" type="submit" name="submit">Đăng ký</button>
							</div>
						</div>
				</form>
				</div>	
			</div>
		</div>
@endsection