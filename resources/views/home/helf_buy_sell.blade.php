<?php $dataHelp = app('App\Http\Controllers\HomeController')->getListHelp(); ?>
<section class="list-guide-form">
	<div class="panel panel-primary mResource">
		<div class="panel-heading">
			<i class="fa fa-newspaper-o" aria-hidden="true"></i> Hướng Dẫn Mua Bán 
		</div>
		<div class="panel-body">
			<div class="row">
				<ul class="news">
					@foreach($dataHelp as $p)
						<li>
							<a href="/guide/{{$p->slug}}" title="{{$p->title}}">{{$p->title}}</a>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</section>