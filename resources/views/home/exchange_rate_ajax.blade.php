<div class="panel-heading rate_update_time" style="background: unset!important;color: unset!important;"><i class="fa fa-clock-o" aria-hidden="true"></i> Tỷ Giá Mua Bán cập nhật: <span id="h">{{$dataRate[1]}}</span> giờ <span id="i">{{$dataRate[2]}}</span> phút <span id="s">{{$dataRate[3]}}</span> giây
	</div>
<div class="panel panel-success">
	<table class="table table-hover table-striped" id="table-conten-rate">
		<tbody>
			<tr class="rate_header">
				<th><i class="fa fa-btc" aria-hidden="true"></i> Coins</th>
				<th>Mua (VNĐ)</th>
				<th>Bán (VNĐ)</th>
			</tr>
			@foreach($dataRate[0] as $p)
			<tr>
				<th>
					<img src="{{$p->img}}" alt="{{$p->name}}" class="icon">
					{{$p->name}}
				</th>
				<th>
					<a>
						<span class="rate_buy" style="background: {{$p->vnd_buy_state_color}}"><i class="fa fa-arrow-up" aria-hidden="true"></i> <span id="btc_rate_buy" class="rate_loading" data-amount="{{$p->vnd_buy}}">{{ number_format($p->vnd_buy, 0)}} ₫</span></span>
					</a>
				</th>
				<th>
					<a>
						<span class="rate_sell" style="background: {{$p->vnd_sell_state_color}}"><i class="fa fa-arrow-down" aria-hidden="true"></i> <span id="btc_rate_sell" class="rate_loading" data-amount="{{$p->vnd_sell}}">{{ number_format($p->vnd_sell, 0)}} ₫</span></span>
					</a>
				</th>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
</div>
