@extends('home.master')

@section('title','Thay đổi mật khẩu')

@section('content_buy_sell')
    <div id="snipper" class="col-sm-12">
						<div class="panel panel-default">
						<ul class="nav nav-tabs user-panel">
								<li>
									<a href="/account">
										Thông tin tài khoản
									</a>
								</li>
								<li class="">
									<a href="/history">
										Lịch sử giao dịch
									</a>
								</li>
								<li class="active">
									<a href="/change-password">
										Thay đổi mật khẩu
									</a>
								</li>
								<li>
									<a href="/logout">
										Đăng xuất
									</a>
								</li>
							</ul>
							<div class="panel-body">
														<form action="/post-change-pass" method="post" class="form-horizontal" style="">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	@if (session()->has('errors'))
		<div id="error" style="color: red">{!! session()->get('errors') !!}</div>
	@endif
	<div class="form-group">
		<label class="col-lg-3 control-label">Mật khẩu cũ</label>
		<div class="col-lg-9">
			<input type="password" class="form-control" name="password_current" placeholder="Mật khẩu cũ của bạn" required="">
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label">Mật khẩu mới</label>
		<div class="col-lg-9">
			<input type="password" class="form-control" name="password_new" placeholder="Mật khẩu mới của bạn" required="">
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label">Nhập lại mật khẩu</label>
		<div class="col-lg-9">
			<input type="password" class="form-control" name="password_new_retype" placeholder="Nhập lại mật khẩu mới của bạn" required="">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-5 col-md-offset-5">
			<button class="btn btn-primary uptext" type="submit" name="btnPassword"><i class="fa fa-floppy-o" aria-hidden="true"></i> Cập nhật</button>
		</div>
	</div>
</form>							</div>	
						</div>
					</div>
@endsection
