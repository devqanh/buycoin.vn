@extends('home.master')

@section('title','Thông tin tài khoản')

@section('content_buy_sell')
    <div id="snipper" class="col-sm-12">
						<div class="panel panel-default">
						<ul class="nav nav-tabs user-panel">
								<li class="active">
									<a href="/account">
										Thông tin tài khoản
									</a>
								</li>
								<li class="">
									<a href="/history">
										Lịch sử giao dịch
									</a>
								</li>
								<li class="">
									<a href="/change-password">
										Thay đổi mật khẩu
									</a>
								</li>
								<li>
									<a href="/logout">
										Đăng xuất
									</a>
								</li>
							</ul>
							<div class="panel-body">
								<h1 class="title-user">
									Quản lý tài khoản								</h1>
														<form action="/post-edit-profile" method="post" class="form-horizontal" style="">
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
	@if (session()->has('errors'))
		<div id="error" style="color: red">{!! session()->get('errors') !!}</div>
	@endif
	<div class="form-group">
		<label class="col-lg-3 control-label">Email</label>
		<div class="col-lg-9">
			<input type="email" class="form-control" value="{{$data->email}}" disabled>
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label">Họ tên</label>
		<div class="col-lg-9">
			<input type="text" name="fullname" class="form-control" placeholder="Họ tên của bạn" required="" autocomplete="off" value="{{$data->name}}">
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label">Số điện thoại</label>
		<div class="col-lg-9">
			<input type="text" name="phone" class="form-control" placeholder="Số điện thoại của bạn" required="" autocomplete="off" value="{{$data->phone}}">
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label">Địa chỉ</label>
		<div class="col-lg-9">
			<input type="text" name="address" class="form-control" placeholder="Địa chỉ của bạn" required="" autocomplete="off" value="{{$data->address}}">
		</div>
	</div>
	<div class="form-group">
		<label class="col-lg-3 control-label"></label>
		<div class="col-lg-9">
			Ngày đăng ký: {{ Carbon\Carbon::parse($data->date_create)->format('d/m/Y H:i') }}		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-5 col-md-offset-5">
			<button class="btn btn-primary uptext" type="submit" name="btnInfo"><i class="fa fa-floppy-o" aria-hidden="true"></i> Cập nhật</button>
		</div>
	</div>
</form>							</div>	
						</div>
					</div>
@endsection