@extends('home.master')

@section('title','Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC')
@section('content_buy_sell')
<div id="snipper" class="col-sm-12">
	<div class="title_container_b"><i class="fa fa-exchange" aria-hidden="true"></i> ĐẶT LỆNH MUA BÁN<span></span> </div>
						<div class="panel panel-success">
							<ul class="nav nav-tabs">
								<li>
									<a href="/" title="Mua Bitcoin">
									<img src="/img/down.png" alt="Mua Bitcoin"> Mua tiền ảo								</a>
								</li>
								<li class="active">
									<a title="Bán Bitcoin">
									<img class="down" src="/img/up.png" alt="Bán Bitcoin"> Bán  tiền ảo								</a>
								</li>
							</ul>
							<div class="panel-body">
								<form action="/register-sell" method="post" class="form-horizontal">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
																			<div id="error"></div>
										<div class="form-group">
											<label class="col-lg-4 control-label labelRed">Số lượng bán</label>
											<div class="col-lg-8">
												<div class="input-group formRed">
													<input id="number-coin" type="number" step="any" pattern="\d+(\.\d{2})?" name="number_sell" class="form-control" placeholder="Số lượng" required autocomplete="off" maxlength="10" onkeyup="calMoney()">
													<span class="input-group-addon">
														<select name="type" id="type-coin" style="float: left;border: 0px;background: #f2dede;width: 100px;" required onchange="calMoney();checkUrlS()">
															@foreach ($data as $p)
															<option value="{{$p->id_coin}}" ammount = "{{$p->vnd_sell}}" min = "{{$p->min_sell}}" max = "{{$p->max_sell}}" lu = "{{strlen($p->address_recive)}}">{{$p->name}}</option>
															@endforeach
														</select>
													</span>
												</div>
												<div class="text-success" id="rate_apply"></div>	
												<div id="num_error"></div>
											</div>
										</div>
										<div class="form-group">
											<label for="bank" class="col-md-4 control-label labelRed">Số tiền</label>
											<div class="col-lg-8">
												<input type="text" id="ammount_cal" class="form-control formRed" placeholder="Chúng tôi sẽ tính số tiền giúp bạn" required disabled>
											</div>
										</div>
										<div class="form-group">
											<label for="bank" class="col-md-4 control-label labelRed">Ngân hàng nhận tiền</label>
											<div class="col-lg-8">
												<div class="btn-group bootstrap-select form-control formRed show-tick">
													<button type="button" class="btn dropdown-toggle btn-danger" data-toggle="dropdown" role="button" data-id="bankrecived" title="Vietcombank">
														<span class="filter-option pull-left"><img class="icon-select" src="/img/vcb.png">Vietcombank</span>&nbsp;<span class="bs-caret"></span></span>
													</button>
												</div>
											</div>
										</div>

										<div class="form-group">
											<label for="bank" class="col-md-4 control-label labelRed">Số tài khoản nhận tiền</label>
											<div class="col-lg-8">
												<input type="text" name="account" class="form-control formRed" placeholder="Số tài khoản bao gồm 13 chữ số" required id="id-account-bank" onclick="checkInMobileA()">
												<i class="fa fa-fw fa-check check-validate-success"></i>
												<div id="bank_error" class="error-msg-account"></div>
											</div>
										</div>
										<div class="form-group">
											<label for="bank" class="col-md-4 control-label labelRed">Địa chỉ ví</label>
											<div class="col-lg-8">
												<input type="text" name="url_coin" id="url-coin"  class="form-control formRed" placeholder="Địa chỉ ví chuyển tiền" required onclick="checkInMobile()">
												<i class="fa fa-fw fa-check check-validate-success"></i>
												<div id="bank_error" class="error-msg-url"></div>
											</div>
										</div>
										<div class="form-group">
											<label for="bank" class="col-md-4 control-label labelRed">
												<input type="checkbox" onclick="checkEnterTag(this)" value="1" name="is_tag"> Destination Tag
											</label>
											<div class="col-lg-8">
												<input type="text" name="tag"  class="form-control formRed" placeholder="Nhập tag nếu có" disabled>
												<span class="account_load" id="a-name-account">
													
												</span>
												<div id="bank_error"></div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12 right">
												<button class="btn btn-danger submit" type="submit" name="submit" onclick="return checkNumber()">Tiếp tục</button>
											</div>
										</div>
								</form>
							</div>
						</div>
					</div>
					<input type="hidden" id="time-work-day" hour_start = "{{$dataConfig->hour_start}}" hour_end = '{{$dataConfig->hour_end}}' hour_start_1 = "{{$dataConfig->hour_start_1}}" hour_end_1 = '{{$dataConfig->hour_end_1}}'>
					<script src="/js/sell_btc.js"></script>
@endsection
