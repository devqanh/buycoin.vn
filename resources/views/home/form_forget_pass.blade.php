@extends('home.master')

@section('title','Quên mật khẩu')


@section('content_buy_sell')
    <div class="col-sm-12">
				<div class="row">
					<div id="snipper" class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
									<div class="title_container"> Quên mật khẩu</div>
							</div>
							<div class="panel-body news_contents">
								<form action="/post-forget-password" method="post" class="form-horizontal">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									@if (session()->has('errors'))
									<div id="error" style="color: red">{!! session()->get('errors') !!}</div>
									@endif
									<div class="form-group">
										<label class="col-lg-3 control-label">Email đăng ký</label>
										<div class="col-lg-9">
											<input type="email" name="email" class="form-control" placeholder="Email của bạn" required="" autocomplete="off">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-5 col-md-offset-5">
											<button class="btn btn-primary uptext" type="submit" name="submit">Gửi yêu cầu</button>
										</div>
									</div>
								</form>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
@section('content_report')
@include('home.helf_buy_sell')
@endsection
@section('content_transaction_info')
@endsection
@section('helf_buy_sell')
@endsection