@extends('home.master')

@section('title')
	Hướng dẫn - trang {{$page}}
@endsection



@section('content_buy_sell')
    <div class="col-sm-12">
				<div class="row">
					<div id="snipper" class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
									<div class="title_container"> Hướng dẫn</div>
							</div>
							<div class="panel-body news_contents">
								@foreach ($data as $p)
								<div class="row news_space">
									<div class="col-sm-4">
										<img src="{{$p->img}}">
									</div>
									<div class="col-sm-8">
										<a href="/guide/{{$p->slug}}">
											<b>{{$p->title}} </b>
										</a>
										<p class="date">
											<i class="fa fa-calendar-check-o" aria-hidden="true"></i>
											{{ Carbon\Carbon::parse($p->date_create)->format('d/m/Y H:i') }}
										</p>
										<p class="contents">{{$p->summary}}</p>
									</div>
									<div class="col-sm-10 col-md-offset-1 clearfix"></div>
								</div>
								@endforeach
								{{ $data->links() }}
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
@section('content_report')
@include('home.helf_buy_sell')
@endsection
@section('content_transaction_info')
@endsection
@section('helf_buy_sell')
@endsection