@extends('home.master')

@section('title')
	Đánh giá dịch vụ
@endsection

@section('content_buy_sell')
    <div class="col-sm-12">
				<div class="row">
					<div id="snipper" class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
									<div class="title_container"> Đánh giá dịch vụ</div>
							</div>
							<div class="panel-body news_contents">
								<div class="pm-card-body ex-home">
									<div id="vutadesign">
										@foreach ($data as $p)
										<div class="wrapper">
										  <div class="itemdiv dialogdiv"><div class="body">
											  <div class="name">
													<span class="link-color">{{$p->phone}}</span>
												  </div>
												  <div class="time">
													<i class="icon-time"></i>
													<span class="green" style="color:#000;">{{ Carbon\Carbon::parse($p->date_create)->format('d/m/Y') }}</span>
												  </div>
												  <div class="clearfix"></div>
												  <div class="text">{{$p->content}}</div>
												</div>
										  </div>
										</div>
										@endforeach
										{{ $data->links() }}
									</div>
								</div>
							</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('content_report')
@endsection
@section('content_transaction_info')
@endsection