<?php $dataReport = app('App\Http\Controllers\HomeController')->getListReport(); ?>
<div class="rateTable">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Đánh giá dịch vụ</div>
		<div id="vutadesign">
			<div class="well reviews">
				@foreach($dataReport as $p)
					<div class="list-reviews">
						<div class="wrapper">
							<div class="itemdiv dialogdiv">
								<div class="body">
									<div class="name">
										<span class="link-color">{{$p->phone}}</span>
									</div>
									<div class="clearfix"></div>
									<div class="text">
										{{$p->content}}
									</div>
									<div class="text">
										{{ Carbon\Carbon::parse($p->date_create)->format('d/m/Y H:i:s') }}
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
		<div class="panel-footer">
			<div class="read-more-box">
				<div class="read-more-link">
					<a href="/reviews">Đọc thêm</a>
				</div>
			</div>
		</div>
	</div>
</div>