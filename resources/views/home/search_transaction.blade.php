@extends('home.master')

@section('title','Tra cứu giao dịch')

@section('content_buy_sell')
    <div class="col-sm-12">
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div id="snipper" class="title_container"> Tra cứu giao dịch</div>
							</div>
							<div class="panel-body">
							<form action="/find-transaction" method="get" class="form-horizontal">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								@isset($data)
								@if ($data->isEmpty())
									<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
								  <span aria-hidden="true">×</span>
									</button><i class="fa fa-hashtag"></i> Mã giao dịch này không tồn tại, vui lòng kiểm tra lại</div>
								@endif
								@endisset	
							<div id="error"></div>
								<div class="form-group">
									<label class="col-lg-3 control-label">Mã giao dịch</label>
									<div class="col-lg-9">
										<input type="text" name="orderid" class="form-control" placeholder="Mã giao dịch của bạn" required="" autocomplete="off">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-5 col-md-offset-5">
										<button class="btn btn-primary uptext submit" type="submit" name="submit">Tra cứu</button>
									</div>
								</div>
							</form>
							<div class="col-sm-12 table-responsive">
							@isset($data)
								@if (!$data->isEmpty())
									<table class="table table-hover table-striped tb-customize">
	<thead>
		<tr>
			<th class="center">Mã ĐH</th>
			<th class="center">Tài khoản</th>
			<th class="center">Loại</th>
			<th>ALT Coin</th>
			<th class="center">Số lượng</th>
			<th class="right">Số tiền</th>
			<th class="center" width="90px">Tình trạng</th>
			<th class="center" width="80px">Thời Gian</th>
			<th class="center">Trạng thái</th>
		</tr>
	</thead>
		<tbody>
			@foreach ($data as $p)
			<tr>
				<td class="center">
					@if ($p->type_action)
						<a href="/detail-order-buy/{{$p->id}}">{{$p->code}}</a>
					@else
						<a href="/detail-order-sell/{{$p->id}}">{{$p->code}}</a>
					@endif
				</td>
				<td>
					{{$p->account_email}}
				</td>
				<td class="center">
					@if ($p->type_action)
						Mua
					@else
						Bán
					@endif
				</td>
				<td class="left">
					<span class="tb-name-coin">{{$p->name}}</span>
				</td>
				<td class="center">
					<span class="tb-number-coin">{{$p->number_coin}}</span>
				</td>
				<td class="right">
					<span class="tb-price-coin">
					@if ($p->type_action == 0)
						@if ($p->money > 0)
						{{ number_format($p->money, 0)}} ₫
						@else
						Đợi chốt giá
						@endif
					@else
						{{ number_format($p->money, 0)}} ₫
					@endif
					</span>
				</td>
				<td class="center">
					<span class="tb-state-coin">
					@if ($p->is_pay)
						<span style="color: #7ad237"> Đã thanh toán </span>
					@else
						<span style="color: red">Chưa thanh toán</span>
					@endif
					</span>
				</td>
				<td class="center">
					{{ Carbon\Carbon::parse($p->time)->format('d/m/Y H:i:s') }}
				</td>
				<td class="center">
					<span class="tb-state-coin">
					@if ($p->state == 0)
						Chưa xử lý
					@elseif ($p->state == 1)
						<span style="color: #7ad237"> Đã xử lý </span>
					@else
						<span style="color: red"> Đã bị hủy </span>
					@endif
				</span>
				</td>
			</tr>
			@endforeach
		</tbody>
</table>
								@endif
							@endisset
						</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
@endsection