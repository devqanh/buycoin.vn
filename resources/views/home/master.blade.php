<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>
		@yield('title')
	</title>
	
	<meta name="theme-color" content="#0088cc">
	<meta name="msapplication-navbutton-color" content="#0088cc">
	<meta name="apple-mobile-web-app-status-bar-style" content="#0088cc">
	<meta name="viewport" content="width=device-width">
	<meta name="Description" content="@if(isset($description)) {{$description}} @else Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC @endif">
  <meta name="Keywords" content="@if(isset($keyword)) {{$keyword}} @else Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC @endif">
  <meta name="Classification" content="Personal">
  <meta name="Rating" content="General">
  <meta name="Revisit-After" content="7 Days">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="DC.Description" content="@if(isset($description)) {{$description}} @else Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC @endif">
	<meta property="og:site_name" content="Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC">
	<meta property="og:image" content="">
	<meta property="og:image:width" content="470px">
	<meta property="og:image:height" content="292px">
	<meta property="og:type" content="website">
	<meta property="og:url" content="https://coinbank247.com/">
	<meta property="og:title" content="Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC">
	<meta property="og:description" content="">
	<meta name="geo.placename" content="Viet Nam">
	<meta name="geo.region" content="vn">
	<meta name="DC.Language" content="vn">
	<script src="/js/jquery.min.js"></script>
	<script src="/js/lib.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/css/swiper.min.css">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap-select.min.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css">
	<link rel="stylesheet" type="text/css" href="/css/custom.css">
	<link href="/img/favicon.png" rel="icon" type="image/x-icon">
</head>
<body>
<header>
	
	<nav class="navbar navbar-default">
	  <div class="container">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="/"><img src="{{$dataConfig->logo}}" class="logo" alt="logo"></a>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav navbar-right">
			<li class=""><a href="/news">Tin tức </a></li>
			<li class=""><a href="/guide">Hướng Dẫn</a></li>
			<li class=""><a href="/find"><i class="fa fa-search" aria-hidden="true"></i> Tra cứu giao dịch</a></li>
			@if (!Auth::check())
		  <li class=""><a href="/dieu-khoan"><i class="fa fa-user-plus" aria-hidden="true"></i> Đăng ký</a></li>
				<li class=""><a href="/login"><i class="fa fa-user-circle" aria-hidden="true"></i> Đăng nhập</a></li>
			@else
			<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user-circle" aria-hidden="true"></i> {{Auth::user()->email}} <span class="caret"></span></a>
				  <ul class="dropdown-menu" role="menu">
					<li><a href="/account"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Thông tin tài khoản</a></li>
					<li><a href="/history"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Lịch sử giao dịch</a></li>
					<li><a href="/change-password"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Đổi mật khẩu</a></li>
					<li><a href="/logout"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Đăng xuất</a></li>
				  </ul>
				</li>
			@endif
		  </ul>
		</div>
	  </div>
	</nav>
</header>
<div id="mSlider" class="carousel slide" data-ride="carousel" data-interval="5000">
	<div class="carousel-inner" role="listbox">
		<div class="item">
			<img src="{{$dataConfig->banner_1}}" alt="">
			<div class="sliderTextOne">
				<div class="MidFixed"></div>
			</div>
		</div>
		<div class="item active">
			<img src="{{$dataConfig->banner_2}}" alt="">
			<div class="sliderTextOne">
				<div class="MidFixed"></div>
			</div>
		</div>
	</div>
  	<a class="left carousel-control" href="#mSlider" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="right carousel-control" href="#mSlider" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>
@if ($dataConfig->notification)
<div style="text-align: center;">
	<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
		</button><i class="fa fa-exclamation"></i> {{$dataConfig->notification}}
	</div>
</div>
@endif
<section class="mContact hidden-xs">
	<div class="container">
		<div class="row">
			<div class="col col-sm-3">
					<div class="mBorder">
							<a href="tel:{{$dataConfig->phone_1}}" title=""><img src="/img/phone-1.png" alt=""><span>{{$dataConfig->phone_1}}</span></a>
					</div>
			</div>
			<div class="col col-sm-3">			
					<div class="mBorder">
							<a href="tel:{{$dataConfig->phone_2}}" title=""><img src="/img/phone-2.png" alt=""><span>{{$dataConfig->phone_2}}</span></a>
					</div>
			</div>
			<div class="col col-sm-3">
					<div class="mBorder">
						<a href="mailto:{{$dataConfig->email}}"><img src="/img/mail.png" alt=""><span>{{$dataConfig->email}}</span></a>
					</div>
			</div>
			<div class="col col-sm-3">
					<div class="mBorder">
							<img src="/img/address.png" alt=""><span>{{$dataConfig->address}}</span>
					</div>
			</div>
		</div>
	</div>
</section>
<div class="container nopadding-right">
	@section('content_left')
	<div class="col-sm-4">
		<div class="row">
			@include('home.exchange_rate')
			<section>
				<div class="panel panel-primary mResource hidden-sm hidden-md hidden-lg">
					<div class="panel-heading">
						<i class="fa fa-phone" aria-hidden="true"></i> Liên Hệ
					</div>
					<div class="panel-body wContact">
						<ul>
							<li>
								<a href="tel:{{$dataConfig->phone_1}}" title=""><img src="/img/phone-1.png" alt=""><span>{{$dataConfig->phone_1}}</span></a>
							</li>
							<li>
								<a href="tel:{{$dataConfig->phone_2}}" title=""><img src="/img/phone-2.png" alt=""><span>{{$dataConfig->phone_2}}</span></a>
							</li>
							<li>
								<a href="mailto:{{$dataConfig->email}}" title=""><img src="/img/mail.png" alt=""><span>{{$dataConfig->email}}</span></a>
							</li>
							<li>
								<img src="/img/address.png" alt=""><span>{{$dataConfig->address}}</span>
							</li>
						</ul>
					</div>
				</div>
			</section>
			@section('content_report')
				@include('home.report')
			@show
			@section('content_transaction_info')
				@include('home.history')
			@show
		</div>
	</div>
	@show
	<div class="col-sm-8 nopadding-right">
		<div class="row">
			<div class="col-sm-12 rateTable">
				<div class="row">
					@section('content_buy_sell')
					
					@show
				</div>
				@section('helf_buy_sell')
					@include('home.helf_buy_sell')
				@show
			</div>
		</div>
		<div class="row widget-main">
		</div>
</div>
</div>
<footer>
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 fDetail">
					<figure class="logo">
						<a href="/" title="Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC">
							<img src="/img/logof.png" alt="Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC">
						</a>
					</figure>

					<figure><strong>Hotline 1</strong>{{$dataConfig->phone_1}}</figure>
					<figure><strong>Hotline 2</strong>{{$dataConfig->phone_2}}</figure>
					<figure>
						<strong>Email</strong>
						<a class="iblock" href="mailto:{{$dataConfig->email}}">{{$dataConfig->email}}</a>
					</figure>
					<figure>
						<strong>Address</strong>
						{{$dataConfig->address}}					</figure>
				</div>
				<div class="col-sm-4">
					<h2>Giới Thiệu</h2>
					<ul class="nav-bottom">
						<li>
							<a href="/">Về chúng tôi</a>
						</li>
						<li>
							<a href="/">Giao dịch đảm bảo</a>
						</li>
						<li>
							<a href="/">Truyền thông</a>
						</li>
						<li>
							<a href="">Chính sách và điều khoản</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-4">
					<h2>Truyền Thông</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright">
		<div class="container center">
			Created by KDVA
		</div>
	</div>
	<div>
		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5ab478a4d7591465c708d525/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script-->
	</div>
	</footer>
	<script src="/js/jquery.min.js"></script>
	<script src="/js/bootstrap-select.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/propeller.min.js"></script>
	<script src="/js/clipboard.js"></script>
	<script src="/js/footer_home.js"></script>
</body>
</html>
	