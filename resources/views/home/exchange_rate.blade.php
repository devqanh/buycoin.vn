<section id="exchange-rate">
	<img src="/img/ajax-loader.gif" alt="load" style="width: 20px;margin-left: calc(50% - 10px);margin-top: 10px;margin-bottom: 10px">
</section>
<script>
	function runGetRateBuySell() {
		$.get('/get-rate-buy-sell', (r) => {
			$('#exchange-rate').html(r)
			setTimeout(function(){
				$('#table-conten-rate span').removeAttr('style')
			}, 1500);
		})
	}
	runGetRateBuySell()
	setInterval(runGetRateBuySell, {{$dataConfig->timeout_load * 1000}});
</script>