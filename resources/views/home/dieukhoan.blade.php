@extends('home.master')

@section('title','Điều khoản')


@section('content_buy_sell')
<div class="col-sm-12">
<div class="row">
	<div id="snipper" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="title_container">Điều khoản</div>
			</div>
			<div class="panel-body news_contents detail-order">
				<p>
					Để tiếp tục đăng ký, bạn phải đồng ý với các điều khoản sau:
				</p>
				<div style="max-height: 400px;
				    overflow-y: auto;
				    float: left;
				    width: 100%;
				    padding: 5px;
				    border: 1px solid #ccc;">
					{!! html_entity_decode($dataConfig->content_dieukhoan) !!}
				</div>
				<p style="margin-top: 10px;float: left;width: 100%">
					<input type="checkbox" name="" id="approve-dk">
					Tôi đã đọc kỹ điều khoản sử dụng của hệ thống
				</p>
				<div class="center cancel-order">
					<button onclick="redirectRegister()" class="btn-cancel-trade btn btn-success">Đăng ký</button>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('content_report')
@include('home.helf_buy_sell')
@endsection
@section('content_transaction_info')
@endsection
@section('helf_buy_sell')
@endsection