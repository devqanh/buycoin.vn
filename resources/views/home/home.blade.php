@extends('home.master')

@section('title','Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC')

@section('content_buy_sell')
    <div id="snipper" class="col-sm-12">
    	<div class="title_container_b"><i class="fa fa-exchange" aria-hidden="true"></i> ĐẶT LỆNH MUA BÁN<span></span> </div>
			<div class="panel panel-success">
				<ul class="nav nav-tabs">
					<li class="active">
						<a title="Mua Bitcoin">
							<img src="/img/down.png" alt="Mua Bitcoin"> Mua tiền ảo									</a>
					</li>
					<li>
						<a href="/sell" title="Bán Bitcoin">
							<img class="down" src="/img/up.png" alt="Bán Bitcoin"> Bán tiền ảo									</a>
					</li>
				</ul>
				<div class="panel-body">
				<form action="/register-buy" method="post" class="form-horizontal">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
													<div id="error"></div>
					<div class="form-group">
						<label class="col-lg-4 control-label labelGreen">Số lượng mua</label>
						<div class="col-lg-8">
						<div class="input-group formGreen">
						  <input type="number" id="number-coin" step="any" pattern="\d+(\.\d{2})?" name="number_buy" class="form-control" placeholder="Số lượng" required autocomplete="off" maxlength="10" onkeyup="calMoney()">
						  <span class="input-group-addon">
							<select name="type" id="type-coin" style="float: left;border: 0px;background: #dff0d8;width: 100px;" required onchange ="calMoney();checkUrlS()">
								@foreach ($data as $p)
								<option value="{{$p->id_coin}}" ammount = "{{$p->vnd_buy}}" min = "{{$p->min_buy}}" max = "{{$p->max_buy}}" lu = "{{strlen($p->address_recive)}}">{{$p->name}}</option>
								@endforeach
							</select>
						  </span>
						</div>
						<div class="text-success" id="rate_apply"></div>	
						<div id="num_error"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="bank" class="col-md-4 control-label labelGreen">Số tiền</label>
						<div class="col-lg-8">
							<input type="text" id="ammount_cal" class="form-control formGreen" placeholder="Chúng tôi sẽ tính số tiền giúp bạn" required disabled>
							<input type="hidden" id="ammount_cal_not_format" required name="money">
						</div>
					</div>
					<div class="form-group">
						<label for="bank" class="col-md-4 control-label labelGreen">Ngân hàng thanh toán</label>
						<div class="col-lg-8">
							<div class="btn-group bootstrap-select show-tick formGreen"><button type="button" class="btn dropdown-toggle btn-success" data-toggle="dropdown" role="button" data-id="banksend" title="Vietcombank"><span class="filter-option pull-left"><img class="icon-select" src="/img/vcb.png">Vietcombank</span>&nbsp;</button></div>
						</div>
					</div>	
					<div class="form-group">
						<label for="bank" class="col-md-4 control-label labelGreen">Địa chỉ ví nhận</label>
						<div class="col-lg-8">
							<input type="text" name="url_coin" id="url-coin" class="form-control formGreen" placeholder="Địa chỉ ví nhận tiền" required onclick="checkInMobile()">
							<i class="fa fa-fw fa-check check-validate-success"></i>
							<div id="bank_error" class="error-msg-url"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="bank" class="col-md-4 control-label labelGreen">
							<input type="checkbox" onclick="checkEnterTag(this)" value="1" name="is_tag"> Destination Tag
						</label>
						<div class="col-lg-8">
							<input type="text" name="tag" class="form-control formGreen" placeholder="Nhập tag nếu có" disabled>
							<span class="account_load"></span>
							<div id="bank_error"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 right">
							<button class="btn btn-success submit" type="submit" name="submit" onclick="return checkNumber()">Tiếp tục</button>
						</div>
					</div>
				</form>
				</div>	
			</div>
			<input type="hidden" id="time-work-day" hour_start = "{{$dataConfig->hour_start}}" hour_end = '{{$dataConfig->hour_end}}' hour_start_1 = "{{$dataConfig->hour_start_1}}" hour_end_1 = '{{$dataConfig->hour_end_1}}'>
		</div>
<script src="/js/buy_btc.js"></script>
@endsection