@extends('home.master')

@section('title','Lịch sử giao dịch')

@section('content_buy_sell')
<div id="snipper" class="col-sm-12">
				<div class="panel panel-default">
				<ul class="nav nav-tabs user-panel">
						<li class="">
							<a href="/account">
								Thông tin tài khoản
							</a>
						</li>
						<li class="active">
							<a href="">
								Lịch sử giao dịch
							</a>
						</li>
						<li class="">
							<a href="/change-password">
								Thay đổi mật khẩu
							</a>
						</li>
						<li>
							<a href="/logout">
								Đăng xuất
							</a>
						</li>
					</ul>
					<div class="panel-body">
												﻿<div class="row">
<div class="col-sm-12 table-responsive">
<table class="table table-hover table-striped tb-customize">
	<thead>
		<tr>
			<th class="center">Mã ĐH</th>
			<th class="center">Loại</th>
			<th width="80px">ALT Coin</th>
			<th class="center">Số lượng</th>
			<th class="right">Số tiền</th>
			<th class="center">Tình trạng</th>
			<th class="center">Thời Gian</th>
			<th class="center">Trạng thái</th>
		</tr>
	</thead>
		<tbody>
			@foreach ($data as $p)
			<tr>
				<td class="center">
					@if ($p->type_action)
						<a href="/detail-order-buy/{{$p->id}}">{{$p->code}}</a>
					@else
						<a href="/detail-order-sell/{{$p->id}}">{{$p->code}}</a>
					@endif
				</td>
				<td class="center">
					@if ($p->type_action)
						Mua
					@else
						Bán
					@endif
				</td>
				<td class="left">
					<span class="tb-name-coin">{{$p->name}}</span>
				</td>
				<td class="center">
					<span class="tb-number-coin">{{$p->number_coin}}</span>
				</td>
				<td class="right">
					<span class="tb-price-coin">
					@if ($p->type_action == 0)
						@if ($p->money > 0)
						{{ number_format($p->money, 0)}} ₫
						@else
						Đợi chốt giá
						@endif
					@else
						{{ number_format($p->money, 0)}} ₫
					@endif
					</span>
				</td>
				<td class="center">
					<span class="tb-state-coin">
					@if ($p->is_pay)
						<span style="color: #7ad237"> Đã thanh toán </span>
					@else
						<span style="color: red">Chưa thanh toán</span>
					@endif
					</span>
				</td>
				<td class="center">
					{{ Carbon\Carbon::parse($p->time)->format('d/m/Y H:i:s') }}
				</td>
				<td class="center">
					<span class="tb-state-coin">
					@if ($p->state == 0)
						Chưa xử lý
					@elseif ($p->state == 1)
						<span style="color: #7ad237"> Đã xử lý </span>
					@else
						<span style="color: red"> Đã bị hủy </span>
					@endif
				</span>
				</td>
			</tr>
			@endforeach
		</tbody>
</table>
			{{ $data->links() }}

			</div>
</div>							</div>	
				</div>
			</div>
@endsection