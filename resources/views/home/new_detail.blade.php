@extends('home.master')

@section('title')
	{{$data->title}}
@endsection

@section('content_buy_sell')
    <div class="col-sm-12">
				<div class="row">
					<div id="snipper" class="col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
									<div class="title_container"> Tin tức</div>
							</div>
							<div class="panel-body news_contents">
								<h1>{{$data->title}}</h1>
								{!! html_entity_decode($data->content) !!}
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
@section('content_report')
@include('home.helf_buy_sell')
@endsection
@section('content_transaction_info')
@endsection
@section('helf_buy_sell')
@endsection