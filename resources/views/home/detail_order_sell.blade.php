@extends('home.master')

@section('title','Bán Bitcoin - Mua Bitcoin, Mua Bán USDT Mua Bán ETH Mua Bán LTC')


@section('content_buy_sell')
<div class="col-sm-12">
<div class="row">
	<div id="snipper" class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="title_container"> Chi tiết đơn hàng</div>
			</div>
			<div class="panel-body news_contents detail-order">
				<div class="step-introduce">
					Bạn đang bán <span>{{$data->number_coin}}</span> {{$dataCoin->name}} cho <span>{{$host}}</span>. Vui lòng thanh toán theo hướng dẫn dưới đây.
				</div>
				<div class="countdown-time">
					<p>
						<i class="fa fa-fw fa-clock-o"></i>
						Thời gian còn lại để thanh toán
					</p>
					<p>
						<b id="cd-h">00 : </b>
						<b id="cd-m">00 : </b>
						<b id="cd-s">00</b>
					</p>
					<p>Hết thời gian thanh toán nếu {{$host}} chưa nhận được thanh toán từ khách hàng thì đơn hàng sẽ bị hủy, Nếu nhận được thanh toán trễ sẽ hoàn tiền lại cho quý khách</p>
				</div>
				@if ($data->state == 0)
				<div class="center cancel-order">
					<a href="/cancel-transaction-buy/{{$data->id}}" class="btn-cancel-trade btn btn-danger" onclick="return confirmCancel()">Hủy bỏ giao dịch</a>
				</div>
				@endif
				<table class="table table-bordered table-striped tb_detail_transaction">
					<tbody>
						<tr style="color: #fff; background: #374758;">
							<td colspan="2" style="font-size: 13px!important">THÔNG TIN THANH TOÁN</td>
						</tr>
						<tr>
							<td width="150px">Phương thức thanh toán</td>
							<td>Chuyển tiền ảo</td>
						</tr>
						<tr>
							<td>Số tiền phải trả</td>
							<td>
								<b style="color: #5cb85c">{{$data->number_coin}} {{$dataCoin->name}}</b>
							</td>
						</tr>
						<tr>
							<td>Ví gửi</td>
							<td>
								Địa chỉ <br>
								<div class="info-url-vi">
									<input type="text" value="{{$data->address_coin}}"> <button onclick="copyToClipboard(this)">Sao chép</button>	
								</div>
								
							</td>
						</tr>
						<tr>
							<td>Ví nhận</td>
							<td>
								Địa chỉ <br>
								<div class="info-url-vi">
									<input type="text" value="{{$dataCoin->address_recive}}"> <button  onclick="copyToClipboard(this)">Sao chép</button>	
								</div> <br>
								@if ($dataCoin->tag != null)
									Destination Tag <br>
									<div class="info-url-vi">
										<input type="text" value="{{$dataCoin->tag}}"> <button  onclick="copyToClipboard(this)">Sao chép</button>	
									</div>
								@endif
							</td>
						</tr>
						<tr>
							<td>Hướng dẫn thanh toán</td>
							<td>Vui lòng chuyển <b style="color: #5cb85c">{{$data->number_coin}} {{$dataCoin->name}}</b> từ "Ví gửi" tới "Ví nhận"
							</td>
						</tr>
						<tr>
							<td>Tình trạng</td>
							<td>
								@if ($data->is_pay)
									<span style="color: #7ad237"> Đã thanh toán </span>
								@else
									<span style="color: red">Chưa thanh toán</span>
								@endif
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<b style="color: red">QUAN TRỌNG <br></b>
								Giao dịch trên <b>{{$host}}</b> yêu cầu độ chính xác. Bạn cần phải gửi số tiền chính xác và sử dụng đúng Nội dung chuyển khoản.<br>
								Phí gửi coin: Người chuyển chịu<br>
								Ví dụ: 	Bạn bán 1 BTC nếu phí gửi 0.0005 BTC thì bạn phải gửi 1.0005 BTC mới đủ số lượng<br>
								Bạn mua 1 BTC nếu phí gửi là 0.001 BTC thì hệ thống sẽ gửi 1.001 BTC cho bạn <br>
								Nếu gửi không chính xác thì đơn hàng sẽ bị hủy và hoàn tiền lại nếu có
								Nếu trong thời gian 180 phút mà chưa nhận được thanh toán thì đơn hàng sẽ bị hủy và hoàn tiền lại nếu có
							</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered table-striped tb_detail_transaction">
					<tbody>
						<tr style="color: #fff; background: #374758;">
							<td colspan="2" style="font-size: 13px!important">TÓM LƯỢC ĐƠN HÀNG: {{$data->code}}</td>
						</tr>
						<tr>
							<td width="150px">Số Tài khoản VCB của khách hàng</td>
							<td>{{$data->account_bank}}</td>
						</tr>
						<tr>
							<td>Số tiền</td>
							<td>
								@if ($data->money > 0)
								{{ number_format($data->money, 0)}} ₫
								@else
								Đợi chốt giá
								@endif
							</td>
						</tr>
						<tr>
							<td>Trạng thái</td>
							<td>
								@if ($data->state == 0)
									Chưa xử lý
								@elseif ($data->state == 1)
									<span style="color: #7ad237"> Đã xử lý </span>
								@else
									<span style="color: red"> Đã bị hủy </span>
								@endif
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<b style="color: red">GHI CHÚ <br></b>
							Sau khi nhận đươc thanh toán từ khách hàng, BQT sẽ chốt giá tại thời điểm nhận được thanh toán, và xử lý chuyển tiền cho quý khách<br>
							Hệ thống chỉ chấp nhận thanh toán qua vietcombank, các hệ thống ngân hàng khác sẽ không được chấp nhận<br>
							Phí chuyển khoản ngân hàng: người chuyển chịu
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<input type="hidden" id="time-count-down-d" h = "{{$h}}" m = "{{$m}}" s = "{{$s}}">
</div>
</div>
<script src="/js/detail_order.js"></script>
@endsection
@section('content_report')
@include('home.helf_buy_sell')
@endsection
@section('content_transaction_info')
@endsection
@section('helf_buy_sell')
@endsection