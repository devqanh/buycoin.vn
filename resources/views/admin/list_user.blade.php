@extends('admin.master')

@section('title','Danh sách tài khoản')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Danh sách tài khoản</span>
		<form class="area-control-register-all" method="GET" action="/admin/search-users">
			<input type="text" name="info" class="input-customize" placeholder="Tìm kiếm">
		</form>
	</div>
	<table class="table-data-customize table-content">
		<thead>
			<tr>
				<th class="center-align" width="50px">TT</th>
				<th>Email</th>
				<th>Tên</th>
				<th>Điện thoại</th>
				<th class="center-align">Trạng thái</th>
			</tr>
		</thead>
		<tbody>
			<?php $index = 1; ?>
			@foreach ($users as $user)
			  <tr>
					<td class="center-align">{{ $index++ }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					@if ($user->state)
					  <td class="center-align" width="80px">
					  	<a href="/adbank/lock-user/{{$user->id}}">
					  		<i class="fa fa-fw fa-lock" title="Khóa tài khoản"></i>
					  	</a>
						</td>
					@else
					 	<td class="center-align" width="80px">
					 		<a href="/adbank/unlock-user/{{$user->id}}" style="color: red">
								<i class="fa fa-fw fa-unlock" title="Mở khóa tài khoản"></i>
							</a>
						</td>
					@endif
					
				</tr>
			@endforeach
		</tbody>
	</table>
	@if (!$isSearch)
		{{ $users->links() }}
	@endif
@endsection