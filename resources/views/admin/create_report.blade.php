@extends('admin.master')

@section('title','Thêm mới đánh giá dịch vụ')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Thêm mới đánh giá dịch vụ</span>
		<i class="title-field-required">(*) là trường bắt buộc</i>
	</div>
	<form class="area-new" method="post" action="/adbank/post-create-report">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="input-field col s12">
			<p>Số điện thoại <span class="field-required">*</span></p>
	    <input type="text" class="input-customize" name="phone" required>
	  </div>
	  <div class="input-field col s12">
			<p>Nội dung <span class="field-required">*</span></p>
	    <input type="text" class="input-customize" name="content" required>
	  </div>
	  <div class="input-field col s12">
	  	<button class="btn waves-effect waves-light right btn-large" type="submit" name="action">Cập nhật<i class="material-icons right">send</i></button>
	  </div>
	</form>
@endsection