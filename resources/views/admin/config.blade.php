@extends('admin.master')

@section('title','Cấu hình')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Cấu hình</span>
		<i class="title-field-required">(*) là trường bắt buộc</i>
	</div>
	<form class="area-new" method="post" action="/adbank/post-config" encType="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="input-field col s12">
			<p>Địa chỉ <span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="place" required value="{{$data->address}}">
	  </div>
	  <div class="input-field col s12">
			<p>Số điện thoại bàn <span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="phone_1" required value="{{$data->phone_1}}">
	  </div>
	  <div class="input-field col s12">
			<p>Số điện thoại di động <span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="phone_2" required value="{{$data->phone_2}}">
	  </div>
	  <div class="input-field col s12">
			<p>Facebook <span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="facebook" required value="{{$data->facebook}}">
	  </div>
	  <div class="input-field col s12">
			<p>Email <span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="email" required value="{{$data->email}}">
	  </div>
	  <div class="input-field col s12">
			<p>Thời gian làm mới <span class="field-required">*</span></p>
	    <input type="number" class="input-customize validate" min="1" name="time_out" required value="{{$data->timeout_load}}">
	  </div>
	  <div class="input-field col s12">
			<p>Thời gian bắt đầu (Sáng)<span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="hour_start" required value="{{$data->hour_start}}" placeholder="hh:mm">
	  </div>
	  <div class="input-field col s12">
			<p>Thời gian kết thúc (Sáng)<span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="hour_end" required value="{{$data->hour_end}}" placeholder="hh:mm">
	  </div>
	  <div class="input-field col s12">
			<p>Thời gian bắt đầu (Chiều)<span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="hour_start_a" required value="{{$data->hour_start_1}}" placeholder="hh:mm">
	  </div>
	  <div class="input-field col s12">
			<p>Thời gian kết thúc (Chiều)<span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="hour_end_a" required value="{{$data->hour_end_1}}" placeholder="hh:mm">
	  </div>
	  <div class="input-field col s12">
			<p>Logo<span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" id="img-logo" onclick="BrowseServer('img-logo')" name="logo" required value="{{$data->logo}}">
	  </div>
	  <div class="input-field col s12">
			<p>Banner 1<span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" id="img-banner-1" onclick="BrowseServer('img-banner-1')" name="banner_1" required value="{{$data->banner_1}}">
	  </div>
	  <div class="input-field col s12">
			<p>Banner 2<span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="banner_2" id="img-banner-2" onclick="BrowseServer('img-banner-2')" required value="{{$data->banner_2}}">
	  </div>
	  <div class="input-field col s12">
			<p>Ngân hàng giao dịch</p>
	    <input type="text" class="input-customize" name="bank_name" required value="{{$data->bank_name}}">
	  </div>
	  <div class="input-field col s12">
			<p>Chi nhánh</p>
	    <input type="text" class="input-customize" name="bank_place" required value="{{$data->bank_place}}">
	  </div>
	  <div class="input-field col s12">
			<p>Chủ tài khoản</p>
	    <input type="text" class="input-customize" name="bank_name_user" required value="{{$data->bank_name_user}}">
	  </div>
	  <div class="input-field col s12">
			<p>Số tài khoản</p>
	    <input type="text" class="input-customize" name="bank_number" required value="{{$data->bank_number}}">
	  </div>
	  <div class="input-field col s12">
			<p>Thông báo <span class="field-required">*</span></p>
	    <textarea  type="text" class="materialize-textarea validate" name="notification">{{$data->notification}}</textarea>
	  </div>
	  <div class="input-field col s12">
			<p>Điều khoản</p>
	    <textarea id="editor"  type="text" class="materialize-textarea validate" name="dieukhoan">{{$data->content_dieukhoan}}</textarea>
	  </div>
	  <div class="input-field col s12">
	  	<button class="btn waves-effect waves-light right btn-large" type="submit" name="action">Cập nhật<i class="material-icons right">send</i></button>
	  </div>
	</form>
	<script>
		CKEDITOR.replace('editor');
		function BrowseServer(elementId) {
	  	CKFinder.popup( {
				chooseFiles: true,
				width: 800,
				height: 600,
				onInit: function( finder ) {
					finder.on( 'files:choose', function( evt ) {
						var file = evt.data.files.first();
						var output = document.getElementById( elementId );
						output.value = file.getUrl();
					} );

					finder.on( 'file:choose:resizedImage', function( evt ) {
						var output = document.getElementById( elementId );
						output.value = evt.data.resizedUrl;
					} );
				}
			} );
   	}
	</script>
@endsection