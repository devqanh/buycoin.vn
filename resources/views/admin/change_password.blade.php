@extends('admin.master')

@section('title','Đổi mật khẩu')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Đổi mật khẩu</span>
	</div>
	<form class="area-new" style="width: 500px" method="post" action="/adbank/post-change-password" encType="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			@if (session()->has('errors'))
			<div class="input-field col s12">
				<div id="error" style="color: red">{!! session()->get('errors') !!}</div>
	  	</div>
			@endif
	  <div class="input-field col s12">
			<p>Mật khẩu cũ <span class="field-required">*</span></p>
	    <input type="password" class="input-customize validate" name="old_pass" required placeholder="Mật khẩu phải chứa ít nhất 8 ký tự">
	  </div>
	  <div class="input-field col s12">
			<p>Mật khẩu mới <span class="field-required">*</span></p>
	    <input type="password" class="input-customize validate" name="new_pass" required>
	  </div>
	  <div class="input-field col s12">
			<p>Nhập lại mật khẩu mới <span class="field-required">*</span></p>
	    <input type="password" class="input-customize validate" name="re_new_pass" required>
	  </div>
	  <div class="input-field col s12">
	  	<button class="btn waves-effect waves-light right btn-large" type="submit" name="action">Cập nhật<i class="material-icons right">send</i></button>
	  </div>
	</form>
	<script>
		$('select').material_select();
	</script>
@endsection