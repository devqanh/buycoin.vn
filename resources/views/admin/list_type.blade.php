@extends('admin.master')

@section('title','Danh sách loại coi')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Danh sách loại coi</span>
		<div class="area-control-register-all">
			<a href="/adbank/type/create" style="margin-top: 7.5px" class="btn-small btn right orange">Tạo mới</a>
		</div>
	</div>
	<table class="table-data-customize table-content">
		<thead>
			<tr>
				<th class="center-align" width="50px">TT</th>
				<th class="center-align">Ảnh</th>
				<th>Id</th>
				<th>Tên</th>
				<th> Tên viết tắt</th>
				<th>Địa chỉ nhận coi</th>
				<th>Tag</th>
				<th class="right-align"> Tỷ giá mua</th>
				<th class="right-align"> Tỷ giá bán</th>
				<th class="center-align"> SL mua nhỏ nhất</th>
				<th class="center-align"> SL mua lớn nhất</th>
				<th class="center-align"> SL bán nhỏ nhất</th>
				<th class="center-align"> SL bán lớn nhất</th>
				<th class="center-align">Trạng thái</th>
				<th class="center-align">#</th>
			</tr>
		</thead>
		<tbody>
			<?php $index = 1; ?>
			@foreach ($data as $p)
			  <tr>
					<td class="center-align">{{ $index++ }}</td>
					<td class="center-align">
						@if ($p->img)
							<img src="{{$p->img}}" width="30px">
						@endif
					</td>
					<td>{{ $p->id_coin }}</td>
					<td>{{ $p->name }}</td>
					<td>{{ $p->symbol }}</td>
					<td>{{ $p->address_recive}}</td>
					<td>{{ $p->tag}}</td>
					<td class="right-align">{{ number_format($p->rate_buy, 0)}}</td>
					<td class="right-align">{{ number_format($p->rate_sell, 0)}}</td>
					<td class="center-align">{{ $p->min_buy}}</td>
					<td class="center-align">{{ $p->max_buy}}</td>
					<td class="center-align">{{ $p->min_sell}}</td>
					<td class="center-align">{{ $p->max_sell}}</td>
					<td class="center-align">
						@if ($p->state)
							<button class="btn-approve btn">Hiện</button>
						@else
							<button class="btn-denied btn">Ẩn</button>
						@endif
					</td>
					<td class="center-align">
						<a href="/adbank/edit-type-coin/{{$p->id}}">
							<i class="fa fa-fw fa-edit" title="Chỉnh sửa"></i>
						</a>
						<a href="/adbank/delete-type-coin/{{$p->id}}">
							<i class="fa fa-fw fa-trash" title="Xóa" onclick="return confirmDelete()"></i>
						</a>
					</td>									
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection