@extends('admin.master')

@section('title','Thêm mới hướng dẫn')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Thêm mới hướng dẫn</span>
		<i class="title-field-required">(*) là trường bắt buộc</i>
	</div>
	<form class="area-new" method="post" action="/adbank/post-create-help" encType="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="input-field col s12">
			<p>Tiêu đề <span class="field-required">*</span></p>
	    <input type="text" class="input-customize validate" name="title" required>
	  </div>
	  <div class="input-field col s12">
			<p>Ảnh đại diện <span class="field-required">*</span></p>
	    <div class="file-field input-field">
        <div class="btn">
          <span>Chọn ảnh</span>
          <input type="file" name="file" accept='image/*' required onchange = 'loadFile(event)'>
        </div>
        <div class="file-path-wrapper" style="opacity: 0">
          <input class="file-path validate" type="text">
        </div>
      </div>
	  </div>
	  <div class="input-field col s12 img-choose" style="display: none">
	  	<p>Ảnh đã chọn</p>
	  	<img src="" style="width: 100px; height: auto">
	  </div>
	  <div class="input-field col s12">
			<p>Tóm tắt <span class="field-required">*</span></p>
	    <textarea  type="text" class="materialize-textarea validate" name="summary" required> </textarea>
	  </div>
	  <div class="input-field col s12">
			<p>Nội dung <span class="field-required">*</span></p>
	    <textarea id="editor" type="text" name="content" required> </textarea>
	  </div>
	  <div class="col s12">
			<p>Trạng thái</p>
	    <div class="switch">
        <label>
          Ẩn
          <input type="checkbox" checked name="state">
          <span class="lever"></span> Hiển thị
        </label>
      </div>
	  </div>
	  <div class="input-field col s12">
	  	<button class="btn waves-effect waves-light right btn-large" type="submit" name="action">Cập nhật<i class="material-icons right">send</i></button>
	  </div>
	</form>
	<script>
		CKEDITOR.replace('editor');
	  loadFile = (event) => {
	  	var numberfile = $('input:file')[0].files.length;
	  	for (let i = 0; i < numberfile; i++) {
				var file = $('input:file')[0].files[i];
				var fileName = file.name;
				var fileExt = '.' + fileName.split('.').pop();
				if (file.size > 1048576 * 2) {
					alert('Ảnh đại diện không được vượt quá 2MB')
					$('input:file').val('')
					$('.img-choose').hide()
					return false;
				}
				if (fileExt != '.jpg' && fileExt != '.png' && fileExt != '.gif' && fileExt != '.jpeg'){
					alert('Không đúng định dạng')
					$('input:file').val('')
					$('.img-choose').hide()
				}
				else {
					$('.img-choose').show()
					$('.img-choose img').attr('src', URL.createObjectURL(event.target.files[i]))
				}
	  	}
		}
	</script>
@endsection