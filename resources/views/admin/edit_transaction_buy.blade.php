@extends('admin.master')

@section('title','Chỉnh sửa giao dịch')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Chỉnh sửa giao dịch</span>
	</div>
	<form class="area-new" style="width: 500px" method="post" action="/adbank/post-edit-transaction-buy/{{$data->id}}" encType="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	  <div class="input-field col s12">
			<p>Số tiền <span class="field-required">*</span></p>
	    <input type="number" class="input-customize validate" name="money" required value="{{$data->money}}">
	  </div>
	  <div class="input-field col s12">
	    <select required name="state_pay">
	      <option value="" disabled selected>Tình trạng</option>
	      <option value="0" @if ($data->is_pay == 0) selected @endif>Chưa thanh toán</option>
	      <option value="1" @if ($data->is_pay == 1) selected @endif>Đã thanh toán</option>
	    </select>
	  </div>
	  <div class="input-field col s12">
	    <select required name="state_transaction">
	      <option value="" disabled selected>Trạng thái</option>
	      <option value="0" @if ($data->state == 0) selected @endif>Chưa xử lý</option>
	      <option value="1" @if ($data->state == 1) selected @endif>Đã xử lý</option>
	      <option value="2" @if ($data->state == 2) selected @endif>Đã bị hủy</option>
	    </select>
	  </div>
	  <div class="input-field col s12">
	  	<button class="btn waves-effect waves-light right btn-large" type="submit" name="action">Cập nhật<i class="material-icons right">send</i></button>
	  </div>
	</form>
	<script>
		$('select').material_select();
	</script>
@endsection