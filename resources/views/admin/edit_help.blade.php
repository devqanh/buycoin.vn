@extends('admin.master')

@section('title','Chỉnh sửa hướng dẫn')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Chỉnh sửa hướng dẫn</span>
		<i class="title-field-required">(*) là trường bắt buộc</i>
	</div>
	<form class="area-new" method="post" action="/adbank/post-edit-help/{{$data->id}}" encType="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="input-field col s12">
			<p>Tiêu đề <span class="field-required">*</span></p>
	    <input type="text" class="input-customize" name="title" required value="{{$data->title}}">
	  </div>
	  <div class="input-field col s12">
			<p>Ảnh đại diện <span class="field-required">*</span></p>
	    <div class="file-field input-field">
        <div class="btn">
          <span>Chọn ảnh</span>
          <input type="file" name="file" accept='image/*' onchange = 'loadFile(event)'>
        </div>
        <div class="file-path-wrapper" style="opacity: 0">
          <input class="file-path" type="text">
        </div>
      </div>
	  </div>
	  <div class="input-field col s12 img-choose">
	  	<p>Ảnh đã chọn</p>
	  	<img src="{{$data->img}}" style="width: 100px; height: auto">
	  </div>
	  <div class="input-field col s12">
			<p>Tóm tắt <span class="field-required">*</span></p>
	    <textarea  type="text" class="materialize-textarea" name="summary" required>{{$data->summary}} </textarea>
	  </div>
	  <div class="input-field col s12">
			<p>Nội dung <span class="field-required">*</span></p>
	    <textarea id="editor" type="text" name="content" required>{{$data->content}} </textarea>
	  </div>
	  <div class="col s12">
			<p>Trạng thái</p>
	    <div class="switch">
        <label>
          Ẩn
          <input type="checkbox" @if ($data->state) checked @endif name="state">
          <span class="lever"></span> Hiển thị
        </label>
      </div>
	  </div>
	  <div class="input-field col s12">
	  	<button class="btn waves-effect waves-light right btn-large" type="submit" name="action">Cập nhật<i class="material-icons right">send</i></button>
	  </div>
	</form>
	<script>
		CKEDITOR.replace('editor');
	  const oldAvatar = $('.img-choose img').attr('src')
	  loadFile = (event) => {
	  	const numberfile = $('input:file')[0].files.length;
	  	for (let i = 0; i < numberfile; i++) {
				const file = $('input:file')[0].files[i];
				const fileName = file.name;
				const fileExt = '.' + fileName.split('.').pop();
				if (file.size > 1048576 * 2) {
					Materialize.toast(msgMaxSize + ' 2MB', timeToast)
					$('input:file').val('')
					$('.img-choose img').attr('src', oldAvata)
					return false;
				}
				if (fileExt != '.jpg' && fileExt != '.png' && fileExt != '.gif' && fileExt != '.jpeg'){
					Materialize.toast(msgNotFormat, timeToast)
					$('input:file').val('')
					$('.img-choose img').attr('src', oldAvatar)
				}
				else{
					$('.img-choose img').attr('src', URL.createObjectURL(event.target.files[i]))
				}
	  	}
		}
	</script>
@endsection