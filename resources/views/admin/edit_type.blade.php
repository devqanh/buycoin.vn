@extends('admin.master')

@section('title','Chỉnh sửa loại coi')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Chỉnh sửa loại coi</span>
	</div>
	<form class="area-new" method="post" action="/adbank/post-edit-type" encType="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" value="{{$id}}" name="id">
	  <div class="input-field col s12">
			<p>Ảnh đại diện </p>
	    <div class="file-field input-field">
        <div class="btn">
          <span>Chọn ảnh</span>
          <input type="file" name="file" accept='image/*' onchange = 'loadFile(event)'>
        </div>
        <div class="file-path-wrapper" style="opacity: 0">
          <input class="file-path validate" type="text">
        </div>
      </div>
	  </div>
	  @if ($img)
	  <div class="input-field col s12 img-choose">
	  	<p>Ảnh đã chọn</p>
	  	<img src="{{$img}}" style="width: 100px; height: auto">
	  </div>
	  @else 
	  <div class="input-field col s12 img-choose" style="display: none">
	  	<p>Ảnh đã chọn</p>
	  	<img src="" style="width: 100px; height: auto">
	  </div>
	  @endif
	  <div class="col s12">
			<p>Trạng thái</p>
	    <div class="switch">
        <label>
          Ẩn
          <input type="checkbox" @if ($state) checked @endif name="state">
          <span class="lever"></span> Hiển thị
        </label>
      </div>
	  </div>
	  <div class="input-field col s12">
	  	<button class="btn waves-effect waves-light right btn-large" type="submit" name="action">Cập nhật<i class="material-icons right">send</i></button>
	  </div>
	</form>
	<script>
	  const oldAvatar = $('.img-choose img').attr('src')
	  loadFile = (event) => {
	  	const numberfile = $('input:file')[0].files.length;
	  	for (let i = 0; i < numberfile; i++) {
				const file = $('input:file')[0].files[i];
				const fileName = file.name;
				const fileExt = '.' + fileName.split('.').pop();
				if (file.size > 1048576) {
					Materialize.toast(msgMaxSize + ' 1MB', timeToast)
					$('input:file').val('')
					$('.img-choose img').attr('src', oldAvata)
					return false;
				}
				if (fileExt != '.jpg' && fileExt != '.png' && fileExt != '.gif' && fileExt != '.jpeg'){
					Materialize.toast(msgNotFormat, timeToast)
					$('input:file').val('')
					$('.img-choose img').attr('src', oldAvatar)
				}
				else{
					$('.img-choose').show()
					$('.img-choose img').attr('src', URL.createObjectURL(event.target.files[i]))
				}
	  	}
		}
	</script>
@endsection