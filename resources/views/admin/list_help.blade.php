@extends('admin.master')

@section('title','Danh sách hướng dẫn')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Danh sách hướng dẫn</span>
		<div class="area-control-register-all">
			<a href="/adbank/create-help" style="margin-top: 7.5px" class="btn-small btn right orange">Tạo mới</a>
		</div>
	</div>
	<table class="table-data-customize table-content">
		<thead>
			<tr>
				<th class="center-align" width="50px">TT</th>
				<th>Ảnh đại diện</th>
				<th>Tiêu đề</th>
				<th>Tóm tắt</th>
				<th class="center-align">Trạng thái</th>
				<th>#</th>
			</tr>
		</thead>
		<tbody>
			<?php $index = 1; ?>
			@foreach ($data as $p)
			  <tr>
					<td class="center-align">{{ $index++ }}</td>
					<td>
						<img src="{{$p->img}}" width="50px" height="50px">
					</td>
					<td>{{ $p->title }}</td>
					<td>{{ $p->summary }}</td>
					<td class="center-align">
						@if ($p->state)
							<button class="btn-approve btn">Hiện</button>
						@else
							<button class="btn-denied btn">Ẩn</button>
						@endif
					</td>
					<td>
						<a href="/adbank/edit-help/{{$p->id}}">
							<i class="fa fa-fw fa-edit" title="Chỉnh sửa"></i>
						</a>
						<a href="/adbank/delete-help/{{$p->id}}" onclick="return confirmDelete()">
							<i class="fa fa-fw fa-trash" title="Xóa"></i>
						</a>
					</td>				
				</tr>
			@endforeach
		</tbody>
	</table>
	{{ $data->links() }}
@endsection