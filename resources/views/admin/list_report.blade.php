@extends('admin.master')

@section('title','Danh sách đánh giá dịch vụ')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Danh sách đánh giá dịch vụ</span>
		<div class="area-control-register-all">
			<a href="/adbank/create-report" style="margin-top: 7.5px" class="btn-small btn right orange">Tạo mới</a>
		</div>
	</div>
	<table class="table-data-customize table-content">
		<thead>
			<tr>
				<th class="center-align" width="50px">TT</th>
				<th class="center-align">Điện thoại</th>
				<th>Nội dung</th>
				<th class="center-align" width="100px">#</th>
			</tr>
		</thead>
		<tbody>
			<?php $index = 1; ?>
			@foreach ($data as $p)
			  <tr>
					<td class="center-align">{{ $index++ }}</td>
					<td class="center-align">{{ $p->phone }}</td>
					<td>{{ $p->content }}</td>
					<td class="center-align">
						<a href="/adbank/edit-report/{{$p->id}}">
							<i class="fa fa-fw fa-edit" title="Chỉnh sửa"></i>
						</a>
						<a href="/adbank/delete-report/{{$p->id}}" onclick="return confirmDelete()">
							<i class="fa fa-fw fa-trash" title="Xóa"></i>
						</a>
					</td>				
				</tr>
			@endforeach
		</tbody>
	</table>
	{{ $data->links() }}
@endsection