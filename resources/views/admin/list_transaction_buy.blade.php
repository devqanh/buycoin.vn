@extends('admin.master')

@section('title','Danh sách giao dịch mua')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Danh sách giao dịch mua</span>
	</div>
	<div style="float: left;width: 100%; overflow-x: auto">		
		<table class="table-data-customize table-content">
			<thead>
				<tr>
					<th class="center-align" width="50px">TT</th>
					<th>Mã</th>
					<th class="center-align">Email</th>
					<th> Điện thoại</th>
					<th> ALT coin</th>
					<th> Địa chỉ ví</th>
					<th> Tag</th>
					<th class="center-align"> Số lượng</th>
					<th> Số tiền</th>
					<th> Tình trạng</th>
					<th class="center-align"> Thời gian</th>
					<th class="center-align"> TG kết thúc</th>
					<th class="center-align"> Trạng thái</th>
					<th class="center-align">#</th>
				</tr>
			</thead>
			<tbody>
				<?php $index = 1; ?>
				@foreach ($data as $p)
				  <tr>
						<td class="center-align">{{ $index++ }}</td>
						<td>{{ $p->code }}</td>
						<td>{{ $p->email }}</td>
						<td>{{ $p->phone }}</td>
						<td>{{ $p->name }}</td>
						<td>
							{{ $p->address_coin }}
						</td>
						<td>{{ $p->tag }}</td>
						<td class="center-align">{{ $p->number_coin }}</td>
						<td>
							@if ($p->money > 0)
							{{ number_format($p->money, 0)}}
							@endif
						</td>
						<td>
							@if ($p->is_pay)
								<span style="color: #7ad237"> Đã thanh toán </span>
							@else
								<span style="color: red">Chưa thanh toán</span>
							@endif
						</td>
						<td class="center">
							{{ Carbon\Carbon::parse($p->time)->format('d/m/Y H:i:s') }}
						</td>
						<td class="center-align">
							{{$p->timeEnd}}
						</td>
						<td class="center">
							@if ($p->state == 0)
								Chưa xử lý
							@elseif ($p->state == 1)
								<span style="color: #7ad237"> Đã xử lý </span>
							@else
								<span style="color: red"> Đã bị hủy </span>
							@endif
						</td>
						<td class="center-align">
							<a href="/adbank/transaction-buy/edit/{{$p->id}}">
								<i class="fa fa-fw fa-edit" title="Chỉnh sửa"></i>
							</a>
							<a href="/adbank/transaction-buy/delete/{{$p->id}}">
								<i class="fa fa-fw fa-trash" title="Xóa" onclick="return confirmDelete()"></i>
							</a>
						</td>									
					</tr>
				@endforeach
			</tbody>
		</table>
		{{ $data->links() }}
	</div>
@endsection