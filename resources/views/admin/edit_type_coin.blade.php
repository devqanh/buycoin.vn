@extends('admin.master')

@section('title','Chỉnh sửa loại tiền')

@section('main')
	<div class="title-module">
		<i class="fa fa-fw fa-table"></i>
		<span>Chỉnh sửa loại tiền</span>
		<i class="title-field-required">(*) là trường bắt buộc</i>
	</div>
	<form class="area-new" method="post" action="/adbank/post-edit-type-coin/{{$data->id}}" encType="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="input-field col s12">
			<p>Id coin <span class="field-required">*</span></p>
	    <input type="text" class="input-customize" name="id_coin" required value="{{$data->id_coin}}">
	  </div>
	  <div class="input-field col s12">
			<p>Tên coin <span class="field-required">*</span></p>
	    <input type="text" class="input-customize" name="name" required value="{{$data->name}}">
	  </div>
	  <div class="input-field col s12">
			<p>Địa chỉ nhận coi </p>
	    <input type="text" class="input-customize" name="address_recive" value="{{$data->address_recive}}">
	  </div>
	  <div class="input-field col s12">
			<p>Tag</p>
	    <input type="text" class="input-customize" name="tag" value="{{$data->tag}}">
	  </div>
	  <div class="input-field col s12">
			<p>Viết tắt <span class="field-required">*</span></p>
	    <input type="text" class="input-customize" name="symbol" required value="{{$data->symbol}}">
	  </div>
	  <div class="input-field col s12">
			<p>Tỷ giá mua <span class="field-required">*</span></p>
	    <input type="number" step="any" class="input-customize" name="rate_buy" required value="{{$data->rate_buy}}">
	  </div>
	   <div class="input-field col s12">
			<p>Tỷ giá bán <span class="field-required">*</span></p>
	    <input type="text" step="any" class="input-customize" name="rate_sell" required value="{{$data->rate_sell}}">
	  </div>
	  <div class="input-field col s12">
			<p>Số lượng mua tối thiểu <span class="field-required">*</span></p>
	    <input type="number" step="any" class="input-customize" name="min_buy" required value="{{$data->min_buy}}">
	  </div>
	  <div class="input-field col s12">
			<p>Số lượng mua tối đa <span class="field-required">*</span></p>
	    <input type="number" step="any" class="input-customize" name="max_buy" required value="{{$data->max_buy}}">
	  </div>
	  <div class="input-field col s12">
			<p>Số lượng bán tối thiểu <span class="field-required">*</span></p>
	    <input type="number" step="any" class="input-customize" name="min_sell" required value="{{$data->min_sell}}">
	  </div>
	  <div class="input-field col s12">
			<p>Số lượng bán tối đa <span class="field-required">*</span></p>
	    <input type="number" step="any" class="input-customize" name="max_sell" required value="{{$data->max_sell}}">
	  </div>
	  <div class="input-field col s12">
			<p>Ảnh đại diện </p>
	    <div class="file-field input-field">
        <div class="btn">
          <span>Chọn ảnh</span>
          <input type="file" name="file" accept='image/*' onchange = 'loadFile(event)'>
        </div>
        <div class="file-path-wrapper" style="opacity: 0">
          <input class="file-path" type="text">
        </div>
      </div>
	  </div>
	  <div class="input-field col s12 img-choose">
	  	<p>Ảnh đã chọn</p>
	  	<img src="{{$data->img}}" style="width: 100px; height: auto">
	  </div>
	  <div class="col s12">
			<p>Trạng thái</p>
	    <div class="switch">
        <label>
          Ẩn
          <input type="checkbox" @if ($data->state) checked @endif checked name="state">
          <span class="lever"></span> Hiển thị
        </label>
      </div>
	  </div>
	  <div class="input-field col s12">
	  	<button class="btn waves-effect waves-light right btn-large" type="submit" name="action">Cập nhật<i class="material-icons right">send</i></button>
	  </div>
	</form>
	<script>
	  const oldAvatar = $('.img-choose img').attr('src')
	  loadFile = (event) => {
	  	const numberfile = $('input:file')[0].files.length;
	  	for (let i = 0; i < numberfile; i++) {
				const file = $('input:file')[0].files[i];
				const fileName = file.name;
				const fileExt = '.' + fileName.split('.').pop();
				if (file.size > 1048576) {
					Materialize.toast(msgMaxSize + ' 1MB', timeToast)
					$('input:file').val('')
					$('.img-choose img').attr('src', oldAvata)
					return false;
				}
				if (fileExt != '.jpg' && fileExt != '.png' && fileExt != '.gif' && fileExt != '.jpeg'){
					Materialize.toast(msgNotFormat, timeToast)
					$('input:file').val('')
					$('.img-choose img').attr('src', oldAvatar)
				}
				else{
					$('.img-choose').show()
					$('.img-choose img').attr('src', URL.createObjectURL(event.target.files[i]))
				}
	  	}
		}
	</script>
@endsection