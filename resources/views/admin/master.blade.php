<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" /> -->
		<title>
			@yield('title')
		</title>
		<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
		<meta name="robots" content="noindex, nofollow" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
<!-- 		<link rel="stylesheet" href="/css/icon.css">
 -->		<link rel="stylesheet" href="/css/font.css">
		<link rel="stylesheet" href="/css/master.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="/css/jquery.timepicker.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-datepicker.css" />
		<link rel="stylesheet" href="/css/materialize.min.css">
		<link href="/css/mbox.min.css" rel="stylesheet">
		<link rel="stylesheet" href="/css/icon.css">
		<link rel="icon" href="/img/favicon.png" />
		<script src="/js/jquery.min.js"></script>
		<script src="/js/materialize.js"></script>
		<script src="/js/mbox-0.0.1.min.js"></script>
		<script src="/js/validator.min.js"></script>
		<script src="/js/lib.js"></script>
		<script src="/ckeditor/ckeditor.js"></script>
		<script src='/js/moment-with-locales.min.js'></script>
		<script type="text/javascript" src="/js/jquery.timepicker.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap-datepicker.js"></script>
		<script src="/ckfinder/ckfinder.js"></script> 
	</head>
	<body>
		<ul id="dropdown-user" class="dropdown-content dropdown-profile">
		  <li><a href="/adbank/change-password">Đổi mật khẩu</a></li>
		  <li><a href="/adbank/logout">Đăng xuất</a></li>
		</ul>
		<nav>
		  <div class="navbar-fixed">
		    <nav>
		      <div class="nav-wrapper">
		        <a href="/" class="brand-logo hide-on-med-and-down"><img src="{{$dataConfig->logo}}"></a>
		        <ul class="right">
		          <li><a class="dropdown-button" href="#!" data-activates="dropdown-user">
		            <span style="font-size: 13px;margin: 0px 10px;">{{Session::get('IS_LOGIN')->user_name}}</span> 
		          	<i class="fa fa-fw fa-angle-down"></i>
		          </a></li>
		        </ul>
		      </div>
		    </nav>
		  </div>
		</nav>
		<div class="row main">
			<div class="menu-left css_menu-left styleScrollBar">
			  <a class="per-group" href="/adbank/users">
		      <span class="pg-title">Quản lý tài khoản</span>
		    </a>
		    <a class="per-group" href="/adbank/news">
		      <span class="pg-title">Quản lý tin tức</span>
		    </a>
		    <a class="per-group" href="/adbank/help">
		      <span class="pg-title">Quản lý hướng dẫn</span>
		    </a>
		    <a class="per-group" href="/adbank/transaction-buy">
		      <span class="pg-title">Quản lý giao dịch (Mua)</span>
		    </a>
		    <a class="per-group" href="/adbank/transaction-sell">
		      <span class="pg-title">Quản lý giao dịch (Bán)</span>
		    </a>
		    <a class="per-group" href="/adbank/type">
		      <span class="pg-title">Quản lý loại</span>
		    </a>
		    <a class="per-group" href="/adbank/report">
		      <span class="pg-title">Quản đánh giá</span>
		    </a>
		    <a class="per-group" href="/adbank/config">
		      <span class="pg-title">Cấu hình</span>
		    </a>
			</div>
			<div class="main-right">
				@section('main')
           
        @show
				<div class="footer">
					Created by KDVA
				</div>
			</div>
		</div>
	</body>
</html>
<script>
	function confirmDelete() {
		var check = confirm('Bạn có chắc chắn muốn xóa ?')
		if (check) {
			return true;
		} else {
			return false;
		}
	}
</script>