<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use Validator;
use Auth;
use Hash;
use View;
use Mail;

class HomeController extends Controller
{ 
  public function __construct(Request $request){
    if(!$request->ajax()){
      $dataConfig = DB::table('tb_config')->first();
      View::share('dataConfig', $dataConfig);
    }
  }
  private function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
  public function getNameAcountBank($id = 0)
  {
    date_default_timezone_set("Asia/Bangkok");
    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', 'https://santienao.com/api/v1/bank_accounts/' . $id);
    $data = json_decode($res->getBody(), true);
    return $data;
  }
  public function sellBTC()
  {
    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/');
    $data = json_decode($res->getBody(), true);
    $dataCoin = DB::table('tb_coin')->get();
    $index = 0;
    foreach ($dataCoin as $p) {
      $dataCoin[$index]->vnd_sell = 0;
      for ($j=0; $j < count($data); $j++) {
        if ($data[$j]['id'] == $dataCoin[$index]->id_coin) {
          $dataCoin[$index]->vnd_sell = $data[$j]['price_usd'] * $dataCoin[$index]->rate_sell;
        }
      }
      $index++;
    }
    return view('home.buy_btc', ['data' => $dataCoin]);
  }
  public function home()
  {
    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/');
    $data = json_decode($res->getBody(), true);
    $dataCoin = DB::table('tb_coin')->get();
    $index = 0;
    foreach ($dataCoin as $p) {
      $dataCoin[$index]->vnd_sell = 0;
      for ($j=0; $j < count($data); $j++) {
        if ($data[$j]['id'] == $dataCoin[$index]->id_coin) {
          $dataCoin[$index]->vnd_buy = $data[$j]['price_usd'] * $dataCoin[$index]->rate_buy;
        }
      }
      $index++;
    }
    return view('home.home', ['data' => $dataCoin]);
  }
  public function account()
  {
   return view('home.manager_user', ['data' => Auth::user(), 'keyword' => 'Thông tin tài khoản', 'description' => 'Thông tin tài khoản']);
  }
  public function historyTransaction()
  {
    date_default_timezone_set("Asia/Bangkok");
    $data = DB::table('tb_sell_buy')->join('tb_coin', 'tb_sell_buy.type_coin', '=', 'tb_coin.id_coin')->where('tb_sell_buy.account_id', Auth::user()->id)->orderBy('tb_sell_buy.id', 'desc')->select('tb_sell_buy.*', 'tb_coin.name')->paginate(7);
    // $index = 0;
    // foreach ($data as $p) {
    //   $data[$index]->isCancel = false;
    //   $m = $p->type_action == 1 ? 5: 180;
    //   if ($p->state == 0) {
    //     $temp = strtotime("now") - strtotime($p->time);
    //     $tempM = 60 * 24 * $temp / 86400;
    //     if ($tempM > $m) {
    //       $data[$index]->isCancel = true;
    //     }
    //   }
    //   $index ++;
    // }
    return view('home.history_transaction_me', ['data' => $data, 'keyword' => 'Lịch sử giao dịch', 'description' => 'Lịch sử giao dịch']);
  }
  public function changePassword()
  {
   return view('home.change_password', ['keyword' => 'Thay đổi mật khẩu', 'description' => 'Thay đổi mật khẩu']);
  }
  public function postChangePass(Request $request)
  {
    $v = Validator::make($request->all(), [
      'password_current' => 'required|min:8|max:255',
      'password_new' => 'required|min:8|max:255',
      'password_new_retype' => 'required|min:8|max:255|same:password_new'
    ]);
    if ($v->fails()){
      $request->session()->flash('errors', 'Vui lòng thử lại');
      return redirect()->back();
    }
    if (!Hash::check($request->password_current, Auth::user()->password)) {
      $request->session()->flash('errors', 'Mật khẩu cũ chưa chính xác');
      return redirect()->back();
    }
    DB::table('tb_user')->where('id', Auth::user()->id)->update(['password' => bcrypt($request->password_current)]);
    return redirect('/account');
  }
  public function register()
  {
    if (Auth::check()) {
      return redirect('/');
    }
    return view('home.register');
  }
  public function searchTransaction()
  {
   return view('home.search_transaction', ['keyword' => 'Tra cứu giao dịch', 'description' => 'Tra cứu giao dịch']);
  }
  public function new(Request $request)
  {
    $data = DB::table('tb_news')->where('state', 1)->orderBy('id', 'desc')->paginate(6);
    $page = $request['page'] != '' ? $request['page'] : 1;
    return view('home.news', ['data' => $data, 'keyword' => 'Tin tức - trang' . $page, 'description' => 'Tin tức - trang ' . $page, 'page' => $page]);
  }
  public function newDetail($slug = null)
  {
    $data = DB::table('tb_news')->where('slug', $slug)->first();
    if (empty($data)) {
      return redirect('/news');
    }
    return view('home.new_detail', ['data' => $data, 'keyword' => $data->tag, 'description' => $data->summary]);
  }
  public function guide(Request $request)
  {
    $data = DB::table('tb_help')->where('state', 1)->orderBy('id', 'desc')->paginate(6);
    $page = $request['page'] != '' ? $request['page'] : 1;
    return view('home.guide', ['data' => $data, 'keyword' => 'Hướng dẫn - trang' . $page, 'description' => 'Hướng dẫn - trang ' . $page, 'page' => $page]);
  }
  public function newGuide($slug = null)
  {
    $data = DB::table('tb_help')->where('slug', $slug)->first();
    if (empty($data)) {
      return redirect('/guide');
    }
    return view('home.guide_detail', ['data' => $data, 'keyword' => $data->title, 'description' => $data->summary]);
  }
  public function getListHelp()
  {
    return DB::table('tb_help')->where('state', 1)->get();
  }
  public function getListRePort()
  {
    return DB::table('tb_report')->orderBy('id', 'desc')->take(5)->get();
  }
  public function getListCoinShow(Request  $request)
  {
    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/');
    $data = json_decode($res->getBody(), true);
    $dataCoin = DB::table('tb_coin')->get();
    // $rateVnd = DB::table('tb_config')->first();
    for ($i=0; $i < count($dataCoin); $i++) {
      $dataCoin[$i]->vnd_sell = '';
      $dataCoin[$i]->vnd_buy = '';
      $dataCoin[$i]->price_usd = '';
      // $dataCoin[$i]->price_btc = '';
      $dataCoin[$i]->rank = 0;
      $dataCoin[$i]->vnd_sell_state_color = '';
      $dataCoin[$i]->vnd_buy_state_color = '';
      for ($j=0; $j < count($data); $j++) {
        if ($data[$j]['id'] == $dataCoin[$i]->id_coin) {
          // $dataCoin[$i]->price_usd = $data[$j]['price_usd'];
          $dataCoin[$i]->vnd_sell = $data[$j]['price_usd'] * $dataCoin[$i]->rate_sell;
          $dataCoin[$i]->vnd_buy = $data[$j]['price_usd'] * $dataCoin[$i]->rate_buy;
          // $dataCoin[$i]->price_btc = $data[$j]['price_btc'];
          $dataCoin[$i]->rank = $data[$j]['rank'];
        }
      }
      if ($request->session()->has('dataRate')) {
        $letOldData = session('dataRate');
        for ($j=0; $j < count($letOldData); $j++) {
          if ($letOldData[$j]->id == $dataCoin[$i]->id) {
            if ($letOldData[$j]->vnd_sell < $dataCoin[$i]->vnd_sell) {
              $dataCoin[$i]->vnd_sell_state_color = '#66e615'; // tang
            }
            if ($letOldData[$j]->vnd_sell > $dataCoin[$i]->vnd_sell) {
              $dataCoin[$i]->vnd_sell_state_color = 'red'; //giam
            }
            if ($letOldData[$j]->vnd_buy < $dataCoin[$i]->vnd_buy) {
              $dataCoin[$i]->vnd_buy_state_color = '#66e615'; // tang
            }
            if ($letOldData[$j]->vnd_buy > $dataCoin[$i]->vnd_buy) {
              $dataCoin[$i]->vnd_buy_state_color = 'red'; //giam
            }
          }
        }
      }
    }
    $dataCoin = $dataCoin->sortBy('rank');
    session(['dataRate' => $dataCoin]);
    return view('home.exchange_rate_ajax', ['dataRate' => [$dataCoin, date("H"), date("i"), date("s")]]);
  }
  public function reviews()
  {
    $data = DB::table('tb_report')->paginate(8);
    return view('home.reviews', ['data' => $data]);
  }
  public function postRegister(Request $request)
  {
    $v = Validator::make($request->all(), [
      'email' => 'required|email|max:255',
      'password' => 'required|min:8|max:255',
      'repassword' => 'required|min:8|max:255|same:password',
      'fullname' => 'required|min:5|max:255',
      'phone' => 'required|min:5|max:255'
    ]);
    if ($v->fails()){
      $request->session()->flash('errors', 'Vui lòng thử lại');
      return redirect()->back();
    }
    $data = DB::table('tb_user')->where('email', $request->email)->first();
    if (!empty($data)) {
      $request->session()->flash('errors', 'Email này đã tồn tại');
      return redirect()->back();
    }
    DB::table('tb_user')->insert(['email' => $request->email, 'password' => bcrypt($request->password), 'name' => $request->fullname, 'phone' => $request->phone]);
    return redirect('/login');
  }
  public function postLogin(Request $request) {
    $rules = [
      'email' =>'required|email',
      'password' => 'required|min:8'
    ];
    $messages = [
      'email.required' => 'Email là trường bắt buộc',
      'email.email' => 'Email không đúng định dạng',
      'password.required' => 'Mật khẩu là trường bắt buộc',
      'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
    ];
    $validator = Validator::make($request->all(), $rules, $messages);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput();
    } else {
      $email = $request->email;
      $password = $request->password;
      if( Auth::attempt(['email' => $email, 'password' =>$password])) {
        return redirect('/');
      } else {
        $request->session()->flash('errors', 'Email hoặc mật khẩu không đúng');
        return redirect()->back();
      }
    }
  }
  public function login()
  {
    if (Auth::check()) {
      return redirect('/');
    }
    return view('home.login');
  }
  public function editProfile(Request $request)
  {
    $v = Validator::make($request->all(), [
      'fullname' => 'required|min:5|max:255',
      'phone' => 'required|min:5|max:255',
      'address' => 'required|min:2|max:255'
    ]);
    if ($v->fails()){
      $request->session()->flash('errors', 'Vui lòng thử lại');
      return redirect()->back();
    }
    DB::table('tb_user')->where('id', Auth::user()->id)->update(['name' => $request->fullname, 'phone' => $request->phone, 'address' => $request->address]);
    return redirect('/account');
  }
  public function Logout()
  {
    auth()->logout();
    return redirect('/');
  }
  public function registerSell(Request $request)
  {
    $v = Validator::make($request->all(), [
      'number_sell' => 'required|max:255',
      'type' => 'required|min:1|max:255',
      'account' => 'required|min:2',
      'url_coin' => 'required|min:2|max:255'
    ]);
    if ($v->fails()){
      return redirect()->back();
    }
    if ($request->is_tag == 1) {
      $v = Validator::make($request->all(), [
        'tag' => 'required|max:255|min:1'
      ]);
      if ($v->fails()){
        return redirect()->back();
      }
      $dataPost = ['account_id' => Auth::user()->id, 'number_coin' => $request->number_sell, 'type_coin' => $request->type, 'account_bank' => $request->account, 'address_coin' => $request->url_coin, 'tag' => $request->tag, 'code' => $this->generateRandomString(), 'time' => date("Y-m-d H:i:s")];
    } else {
      $dataPost = ['account_id' => Auth::user()->id, 'number_coin' => $request->number_sell, 'type_coin' => $request->type, 'account_bank' => $request->account, 'address_coin' => $request->url_coin, 'tag' => null, 'code' => $this->generateRandomString(), 'time' => date("Y-m-d H:i:s")];
    }
    $id = DB::table('tb_sell_buy')->insertGetId($dataPost);
    return redirect('/detail-order-sell/' . $id);
  }
  public function registerBuy(Request $request)
  {
    $v = Validator::make($request->all(), [
      'number_buy' => 'required|max:255',
      'type' => 'required|min:1|max:255',
      'url_coin' => 'required|min:2|max:255',
      'money' => 'required',
    ]);
    if ($v->fails()){
      return redirect()->back();
    }
    if ($request->is_tag == 1) {
      $v = Validator::make($request->all(), [
        'tag' => 'required|max:255|min:1'
      ]);
      if ($v->fails()){
        return redirect()->back();
      }
      $dataPost = ['account_id' => Auth::user()->id, 'number_coin' => $request->number_buy, 'type_coin' => $request->type, 'type_action' => 1, 'tag' => $request->tag, 'address_coin' => $request->url_coin, 'money' => $request->money, 'code' => $this->generateRandomString(), 'time' => date("Y-m-d H:i:s")];
    } else {
      $dataPost = ['account_id' => Auth::user()->id, 'number_coin' => $request->number_buy, 'type_coin' => $request->type, 'type_action' => 1, 'address_coin' => $request->url_coin, 'money' => $request->money, 'code' => $this->generateRandomString(), 'time' => date("Y-m-d H:i:s")];
    }
    $id = DB::table('tb_sell_buy')->insertGetId($dataPost);
    return redirect('/detail-order-buy/'.$id);
  }
  public function getHistoryTransaction()
  {
    // date_default_timezone_set("Asia/Bangkok");
    // $client = new \GuzzleHttp\Client();
    // $res = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/');
    // $data = json_decode($res->getBody(), true);
    // $img = DB::table('tb_img_coin')->get();
    // $dataSend = [];
    // for ($i=0; $i < count($data); $i++) { 
    //   $data[$i]['icon'] = false;
    //   foreach ($img as $c) {
    //     if ($data[$i]['id'] == $c->id_coin) {
    //       $data[$i]['icon'] = $c->img;
    //     }
    //   }
    // }
    $dataCoin = DB::table('tb_coin')->get();
    $dataG = DB::table('tb_sell_buy')->where('state', 1)->orderBy('id', 'desc')->take(5)->get();
    for ($i=0; $i < count($dataG); $i++) { 
      for ($j=0; $j < count($dataCoin); $j++) { 
        if ($dataG[$i]->type_coin == $dataCoin[$j]->id_coin) {
          $dataG[$i]->icon = $dataCoin[$j]->img;
          $dataG[$i]->symbol = $dataCoin[$j]->symbol;
        }
      }
    }
    return $dataG;
  }
  public function getConfig()
  {
    return DB::table('tb_config')->first();
  }
  public function postSearch(Request $request)
  {
    $info = Input::get('orderid', 0);
    date_default_timezone_set("Asia/Bangkok");
    $data = DB::table('tb_sell_buy')
                  ->join('tb_coin', 'tb_sell_buy.type_coin', '=', 'tb_coin.id_coin')
                  ->join('tb_user', 'tb_sell_buy.account_id', '=', 'tb_user.id')
                  ->where('tb_sell_buy.code', $info)
                  ->select('tb_sell_buy.*', 'tb_coin.name', 'tb_user.email as account_email')
                  ->get();
    $index = 0;
    foreach ($data as $p) {
      $data[$index]->isCancel = false;
      if ($p->state == 0) {
        $temp = strtotime("now") - strtotime($p->time);
        $tempM = 60 * 24 * $temp / 86400;
        if ($tempM > 5) {
          $data[$index]->isCancel = true;
        }
      }
      $index ++;
    }
    // $data = DB::table('tb_sell_buy')->join('tb_user', 'tb_sell_buy.account_id', '=', 'tb_user.id')->where('tb_sell_buy.id', $info)->select('tb_sell_buy.*', 'tb_user.name')->get();
    return view('home.search_transaction', ['data' => $data, 'keyword' => 'Tra cứu giao dịch', 'description' => 'Tra cứu giao dịch']);
  }
  public function detailOrderBuy($id = 0, Request $request)
  {
    $data = DB::table('tb_sell_buy')->where('id', $id)->where('type_action', 1)->first();
    if (empty(($data))) {
      return redirect('/');
    }
    $dataCoin = DB::table('tb_coin')->where('id_coin', $data->type_coin)->select('name')->first();
    if (empty(($dataCoin))) {
      return redirect('/');
    }
    $config = DB::table('tb_config')->first();
    $temp = 5*60 - (strtotime('now') - strtotime($data->time));
    if ($temp > 0) {
      $hour = intval($temp / 3600);
      $sDu = $temp % 3600;
      $minute = intval($sDu / 60);
      $sDu = intval($sDu % 60);
    } else {
      $hour = 0;
      $minute = 0;
      $sDu = 0;
    }
    return view('home.detail_order_buy', ['data' => $data, 'config' => $config, 'host' => $request->getHttpHost(), 'h' => $hour, 'm' => $minute, 's' => $sDu, 'dataCoin' => $dataCoin]);
  }
  public function detailOrderSell($id = 0, Request $request)
  {
    $data = DB::table('tb_sell_buy')->where('id', $id)->where('type_action', 0)->first();
    if (empty(($data))) {
      return redirect('/');
    }
    $dataCoin = DB::table('tb_coin')->where('id_coin', $data->type_coin)->select('address_recive', 'name', 'tag')->first();
    if (empty($dataCoin)) {
      return redirect('/');
    }
    $config = DB::table('tb_config')->first();
    $temp = 180*60 - (strtotime('now') - strtotime($data->time));
    if ($temp > 0) {
      $hour = intval($temp / 3600);
      $sDu = $temp % 3600;
      $minute = intval($sDu / 60);
      $sDu = intval($sDu % 60);
    } else {
      $hour = 0;
      $minute = 0;
      $sDu = 0;
    }
    return view('home.detail_order_sell', ['data' => $data, 'config' => $config, 'host' => $request->getHttpHost(), 'h' => $hour, 'm' => $minute, 's' => $sDu, 'dataCoin' => $dataCoin]);
  }
  public function cancelTranSactionBuy($id = 0)
  {
    DB::table('tb_sell_buy')->where('id', $id)->where('account_id', Auth::user()->id)->update(['state' => 2]);
    return redirect('/detail-order-buy/' . $id);
  }
  public function dieukhoan()
  {
    return view('home.dieukhoan');
  }
  public function regetPass()
  {
    if (Auth::check()) {
      return redirect('/');
    }
    return view('home.form_forget_pass');
  }
  public function forgetPass(Request $request)
  {
    $v = Validator::make($request->all(), [
      'email' => 'required|email|max:255',
    ]);
    if ($v->fails()){
      $request->session()->flash('errors', 'Vui lòng thử lại');
      return redirect()->back();
    }
    $data = DB::table('tb_user')->where('email', $request->email)->first();
    if (empty($data)) {
      $request->session()->flash('errors', 'Email không tồn tại');
      return redirect()->back();
    }
    Mail::send('home.forget_pass', ['email' => base64_encode((base64_encode($data->email))), 'host' => env('APP_URL', 'localhost')], function($message) use ($request) {
      $message->to($request->email, 'Visitor')->subject('Lấy lại mật khẩu');
    });
    $request->session()->flash('errors', 'Chúng tôi đã gửi một đường dẫn để thay đổi mật khẩu vào email của bạn. Vui lòng kiểm tra hộp thư đến hoặc spam... để có thể cài đặt lại mật khẩu');
    return redirect()->back();
  }
  public function resetPassword($email = 'xxx-xx-xx')
  {
    if (Auth::check()) {
      return redirect('/');
    }
    return view('home.form_reset_pass', ['email' => $email]);
  }
  public function postResetPassword(Request $request)
  {
    $v = Validator::make($request->all(), [
      'email' => 'required|max:255',
      'password' => 'required|min:8|max:255',
      're_password' => 'required|min:8|max:255|same:password',
    ]);
    if ($v->fails()){
      $request->session()->flash('errors', 'Vui lòng thử lại');
      return redirect()->back();
    }
    DB::table('tb_user')->where('email', base64_decode(base64_decode($request->email)))->update(['password' => bcrypt($request->password)]);
    $request->session()->flash('errors', 'Đổi mật khẩu thành công');
    return redirect()->back();
  }
}