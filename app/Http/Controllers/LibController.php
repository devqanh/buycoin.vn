<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Session;
class LibController extends Controller{
   public static function fommat_string($str){
      $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
      $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
      $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
      $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
      $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
      $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
      $str = preg_replace('/(đ)/', 'd', $str);
      $str = preg_replace('/(A|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/', 'a', $str);
      $str = preg_replace('/(E|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/', 'e', $str);
      $str = preg_replace('/(I|Ì|Í|Ị|Ỉ|Ĩ)/', 'i', $str);
      $str = preg_replace('/(O|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/', 'o', $str);
      $str = preg_replace('/(U|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/', 'u', $str);
      $str = preg_replace('/(Y|Ỳ|Ý|Ỵ|Ỷ|Ỹ)/', 'y', $str);
      $str = preg_replace('/(Đ|D)/', 'd', $str);
      $str = preg_replace('/(S)/', 's', $str);
      $str = preg_replace('/(B)/', 'b', $str);
      $str = preg_replace('/(C)/', 'c', $str);
      $str = preg_replace('/(F)/', 'f', $str);
      $str = preg_replace('/(G)/', 'g', $str);
      $str = preg_replace('/(H)/', 'h', $str);
      $str = preg_replace('/(J)/', 'j', $str);
      $str = preg_replace('/(K)/', 'k', $str);
      $str = preg_replace('/(L)/', 'l', $str);
      $str = preg_replace('/(M)/', 'm', $str);
      $str = preg_replace('/(N)/', 'n', $str);
      $str = preg_replace('/(P)/', 'p', $str);
      $str = preg_replace('/(Q)/', 'q', $str);
      $str = preg_replace('/(R)/', 'r', $str);
      $str = preg_replace('/(T)/', 't', $str);
      $str = preg_replace('/(X)/', 'x', $str);
      $str = preg_replace('/(V)/', 'v', $str);
      $str = preg_replace('/(W)/', 'w', $str);
      $str = preg_replace('/(Z)/', 'z', $str);
      $str = preg_replace('/ /', '-', $str);
      $str = preg_replace('/[^a-zA-Z0-9\-]/','',$str);
      $str = preg_replace('/----/','-',$str);
      $str = preg_replace('/---/','-',$str);
      $str = preg_replace('/--/','-',$str);
      return $str;
   }
   public function deleteSpace($str)
   {
      $str = preg_replace('/ /','-',$str);
      return $str;
   }
   public function AddSpace($str)
   {
      $str = preg_replace('/-/',' ',$str);
      return $str;
   }
   public function separate($chuoi , $kitu){
      $n = strlen($chuoi);
      $row = array();
      $temp = '';
      for ($i = 0; $i<$n; $i++){
         $temp.= $chuoi[$i];
         if ($chuoi[$i] == $kitu || $i == $n-1 ){ $row[] = trim(trim($temp,$kitu)) ; $temp = ''; }
      }
      return $row;
   }
   public function cutstring($str,$length){
      if(strlen($str) > $length) $str = mb_substr($str,0,$length,'UTF-8').'...';
      return $str;
   }
}
