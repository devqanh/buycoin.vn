<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use Validator;
use Session;
use View;

class AdminController extends Controller
{
  public function __construct(Request $request){
    if(!$request->ajax()){
      $dataConfig = DB::table('tb_config')->first();
      View::share('dataConfig', $dataConfig);
    }
  }
  public function login(Request $request)
  {
    if ($request->session()->exists('IS_LOGIN')){
      return redirect('/adbank');
    }
    return view('admin.login');
  }
  public function postLogin(Request $request)
  {
    $v = Validator::make($request->all(), [
      'user' => 'required|max:255|min:2',
      'password' => 'required|min:2',
    ]);
    if ($v->fails()){
      return redirect()->back();
    }
    $data = DB::table('tb_account_admin')->get();
    foreach ($data as $p) {
      if ($p->user_name == $request->user && $p->password == $request->password) {
        session(['IS_LOGIN' => $p]);
        return redirect('/adbank');
      }
    }
    $request->session()->flash('errors', 'Tên đăng nhập hoặc mật khẩu không đúng');
    return redirect('/login-admin');
  }
  public function logout(Request $request)
  {
    $request->session()->flush();
    return redirect('/login-admin');
  }
  public function changePassword()
  {
    return view('admin.change_password');
  }
  public function postChangePass(Request $request)
  {
    $v = Validator::make($request->all(), [
      'old_pass' => 'required|min:8|max:255',
      'new_pass' => 'required|min:8|max:255',
      're_new_pass' => 'required|min:8|max:255|same:new_pass'
    ]);
    if ($v->fails()){
      $request->session()->flash('errors', 'Vui lòng thử lại');
      return redirect()->back();
    }
    $data = DB::table('tb_account_admin')->where('id', Session::get('IS_LOGIN')->id)->first();
    if (empty($data) || $data->password != $request->old_pass) {
      $request->session()->flash('errors', 'Vui lòng thử lại');
      return redirect()->back();
    }
    DB::table('tb_account_admin')->where('id', Session::get('IS_LOGIN')->id)->update(['password' => $request->new_pass]);
    $request->session()->flash('errors', 'Đổi mật khẩu thành công');
    return redirect()->back();
  }
  public function listUsers()
  {
    $info = Input::get('info', false);
    $isSearch = false;
    if ($info != false) {
      $users = DB::table('tb_user')
                      ->where('name', 'like', '%' . $info . '%')
                      ->orwhere('email', 'like', '%' . $info. '%')
                      ->orwhere('phone', 'like', '%'. $info . '%')
                      ->orwhere('id', 'like', '%' . $info . '%')
                      ->get();
      $isSearch = true;         
    } else {
      $users = DB::table('tb_user')->paginate(20);
    }
    return view('admin.list_user', ['users' => $users, 'isSearch' => $isSearch]);
  }
  public function lockUser($id = 0)
  {
    DB::table('tb_user')
          ->where('id', $id)
          ->update(['state' => 0]);
    return redirect('adbank/users');
  }
  public function unlockUser($id = 0)
  {
    DB::table('tb_user')
          ->where('id', $id)
          ->update(['state' => 1]);
    return redirect('adbank/users');
  }
  public function createNew()
  {
    return view('admin.create_new');
  }
  public function postCreateNew(Request $request)
  {
    $v = Validator::make($request->all(), [
      'title' => 'required|max:255|min:2',
      'file' => 'required',
      'summary' => 'required|min:2',
      'content' => 'required|min:2'
    ]);
    if ($v->fails()){
      return redirect()->back();
    } else {
      $imageName = time().'.'.request()->file->getClientOriginalExtension();
      if (request()->file->move(public_path('img'), $imageName)) {
        $title_slug = app('App\Http\Controllers\LibController')->fommat_string($request->title). '.html';
        $tag = '';
        $tag_slug = '';
        if ($request->tag != '') {
          $tag_temp = json_decode($request->tag, true);
          for ($i=0; $i < count($tag_temp) ; $i++) { 
            $tag .= $tag_temp[$i]['tag'].'#';
            $tag_slug .=  app('App\Http\Controllers\LibController')->fommat_string($tag_temp[$i]['tag']).'#';
          }
        }
        $tag = $tag != '' ? $tag : null;
        $tag_slug = $tag_slug != '' ? $tag_slug : null;
        $state = $request->state == 'on' ? 1 : 0;
        DB::table('tb_news')->insert(
          ['title' => $request->title, 'img' => '/img/'. $imageName, 'summary' => $request->summary, 'content' => $request->content, 'slug' => $title_slug, 'tag' => $tag, 'tag_slug' => $tag_slug, 'state' => $state]
        );
        return redirect('adbank/news');
      } else {
        return redirect()->back();
      }
    }
  }
  public function editNew($id = 0)
  {
    $data = DB::table('tb_news')->where('id', '=', $id)->first();
    if (!empty($data)) {
      $tag = [];
      if ($data->tag != null) {
        $tag = explode("#",$data->tag);
      }
      return view('admin.edit_new', ['data' => $data, 'tag' => $tag]);
    } else {
      echo 'Not find infomations';
    }  
  }
  public function postEditNew($id = 0, Request $request)
  {
    $v = Validator::make($request->all(), [
      'title' => 'required|max:255|min:2',
      'summary' => 'required|min:2',
      'content' => 'required|min:2'
    ]);
    if ($v->fails()){
      return redirect()->back();
    } else {
      $title_slug = app('App\Http\Controllers\LibController')->fommat_string($request->title). '.html';
      $tag = '';
      $tag_slug = '';
      if ($request->tag != '') {
        $tag_temp = json_decode($request->tag, true);
        for ($i=0; $i < count($tag_temp) ; $i++) { 
          $tag .= $tag_temp[$i]['tag'].'#';
          $tag_slug .=  app('App\Http\Controllers\LibController')->fommat_string($tag_temp[$i]['tag']).'#';
        }
      }
      $tag = $tag != '' ? $tag : null;
      $tag_slug = $tag_slug != '' ? $tag_slug : null;
      $state = $request->state == 'on' ? 1 : 0;
      $dataPost = ['title' => $request->title, 'summary' => $request->summary, 'content' => $request->content, 'slug' => $title_slug, 'tag' => $tag, 'tag_slug' => $tag_slug, 'state' => $state];
      if ($request->hasFile('file')) {
        $imageName = time().'.'.request()->file->getClientOriginalExtension();
        if (request()->file->move(public_path('img'), $imageName)) {
          $dataPost['img'] = '/img/'. $imageName;
          DB::table('tb_news')->where('id', $id)->update($dataPost);
        } else {
          return redirect()->back();
        }
      } else {
        DB::table('tb_news')->where('id', $id)->update($dataPost);
      }
      return redirect('adbank/news');
    }
  }
  public function deleteNew($id = 0)
  {
    DB::table('tb_news')->where('id', $id)->delete();
    return redirect('adbank/news');
  }
  public function listNew()
  {
    $data = DB::table('tb_news')->orderBy('id', 'desc')->paginate(20);
    return view('admin.list_news', ['data' => $data]);
  }
  public function listType()
  {
    // $client = new \GuzzleHttp\Client();
    // $res = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/');
    // $data = json_decode($res->getBody(), true);
    $dataCoin = DB::table('tb_coin')->get();
    // $rateVnd = DB::table('tb_config')->first();
    for ($i=0; $i < count($dataCoin); $i++) {
      // $dataCoin[$i]->vnd_sell = $dataCoin[$i]->rate_sell;
      // $dataCoin[$i]->vnd_buy = '';
      // $dataCoin[$i]->price_usd = '';
      // $dataCoin[$i]->price_btc = '';
      // $dataCoin[$i]->rank = 0;
      // for ($j=0; $j < count($data); $j++) {
      //   if ($data[$j]['id'] == $dataCoin[$i]->id_coin) {
      //     $dataCoin[$i]->price_usd = $data[$j]['price_usd'];
      //     $dataCoin[$i]->vnd_sell = $data[$j]['price_usd'] * $rateVnd->rate_vnd_sell;
      //     $dataCoin[$i]->vnd_buy = $data[$j]['price_usd'] * $rateVnd->rate_vnd_buy;
      //     $dataCoin[$i]->price_btc = $data[$j]['price_btc'];
      //     $dataCoin[$i]->rank = $data[$j]['rank'];
      //   }
      // }
    }
    $dataCoin = $dataCoin->sortBy('rank');
    return view('admin.list_type', ['data' => $dataCoin]);
  }
  public function editType($id = null)
  {
    $state = DB::table('tb_coin_show')->where('id_coin', $id)->first();
    $img = DB::table('tb_img_coin')->where('id_coin', $id)->first();
    $img_label = false;
    $state_label = false;
    if (!empty($state)) {
      $state_label = true;
    }
    if (!empty($img)) {
      $img_label = $img->img;
    }
    return view('admin.edit_type', ['state' => $state_label, 'img' => $img_label, 'id' => $id]);
  }
  public function postEditType(Request $request)
  {
    $v = Validator::make($request->all(), [
      'id' => 'required|max:255|min:2'
    ]);
    if ($v->fails()){
      return redirect()->back();
    }
    if ($request->hasFile('file')) {
      $imageName = time().'.'.request()->file->getClientOriginalExtension();
      if (request()->file->move(public_path('img'), $imageName)) {
        DB::table('tb_img_coin')->where('id_coin', $request->id)->delete();
        DB::table('tb_img_coin')->insert(['id_coin' => $request->id, 'img' => '/img/' . $imageName]);
      }
    }
    DB::table('tb_coin_show')->where('id_coin', $request->id)->delete();
    if ($request->state == 'on') {
      DB::table('tb_coin_show')->insert(['id_coin' => $request->id]);
    }
    return redirect('adbank/type');
  }
  public function createHelp()
  {
    return view('admin.create_help');
  }
  public function postCreateHelp(Request $request)
  {
    $v = Validator::make($request->all(), [
      'title' => 'required|max:255|min:2',
      'file' => 'required',
      'content' => 'required|min:2',
      'summary' => 'required|min:2'
    ]);
    if ($v->fails()){
      return redirect()->back();
    } else {
      $imageName = time().'.'.request()->file->getClientOriginalExtension();
      if (request()->file->move(public_path('img'), $imageName)) {
        $title_slug = app('App\Http\Controllers\LibController')->fommat_string($request->title) . '.html';
        $state = $request->state == 'on' ? 1 : 0;
        DB::table('tb_help')->insert(
          ['title' => $request->title, 'img' => '/img/'. $imageName,'content' => $request->content, 'slug' => $title_slug, 'state' => $state, 'summary' => $request->summary]
        );
        return redirect('adbank/help');
      } else {
        return redirect()->back();
      }
    }
  }
  public function listHelp()
  {
    $data = DB::table('tb_help')->orderBy('id', 'desc')->paginate(20);
    return view('admin.list_help', ['data' => $data]);
  }
  public function editHelp($id = 0)
  {
    $data = DB::table('tb_help')->where('id', '=', $id)->first();
    if (!empty($data)) {
      return view('admin.edit_help', ['data' => $data]);
    } else {
      echo 'Not find infomations';
    }  
  }
  public function postEditHelp($id = 0, Request $request)
  {
    $v = Validator::make($request->all(), [
      'title' => 'required|max:255|min:2',
      'content' => 'required|min:2',
      'summary' => 'required|min:2'
    ]);
    if ($v->fails()){
      return redirect()->back();
    } else {
      $title_slug = app('App\Http\Controllers\LibController')->fommat_string($request->title) . '.html';
      $state = $request->state == 'on' ? 1 : 0;
      $dataPost = ['title' => $request->title, 'content' => $request->content, 'slug' => $title_slug, 'state' => $state, 'summary' => $request->summary];
      if ($request->hasFile('file')) {
        $imageName = time().'.'.request()->file->getClientOriginalExtension();
        if (request()->file->move(public_path('img'), $imageName)) {
          $dataPost['img'] = '/img/'. $imageName;
          DB::table('tb_help')->where('id', $id)->update($dataPost);
        } else {
          return redirect()->back();
        }
      } else {
        DB::table('tb_help')->where('id', $id)->update($dataPost);
      }
      return redirect('adbank/help');
    }
  }
  public function deleteHelp($id = 0)
  {
    DB::table('tb_help')->where('id', $id)->delete();
    return redirect('adbank/help');
  }
  public function listReport()
  {
    $data = DB::table('tb_report')->paginate(20);
    return view('admin.list_report', ['data' => $data]);
  }
  public function createReport()
  {
    return view('admin.create_report');
  }
  public function postCreateReport(Request $request)
  {
    $v = Validator::make($request->all(), [
      'phone' => 'required|max:255|min:2',
      'content' => 'required|min:2'
    ]);
    if ($v->fails()){
      return redirect()->back();
    } else {
      $dataPost = ['phone' => $request->phone, 'content' => $request->content];
      DB::table('tb_report')->insert($dataPost);
      return redirect('adbank/report');
    }
  }
  public function editReport($id = 0)
  {
    $data = DB::table('tb_report')->where('id', $id)->first();
    if (!empty($data)) {
      return view('admin.edit_report', ['data' => $data]);
    } else {
       echo 'Not find infomations';
    }
  }
  public function postEditReport($id = 0, Request $request)
  {
    $v = Validator::make($request->all(), [
      'phone' => 'required|max:255|min:2',
      'content' => 'required|min:2'
    ]);
    if ($v->fails()){
      return redirect()->back();
    } else {
      $dataPost = ['phone' => $request->phone, 'content' => $request->content];
      DB::table('tb_report')->where('id', $id)->update($dataPost);
      return redirect('adbank/report');
    }
  }
  public function deleteReport($id = 0)
  {
    DB::table('tb_report')->where('id', $id)->delete();
    return redirect('adbank/report');
  }
  public function createTypeCoin()
  {
    return view('admin.create_type_coin');
  }
  public function postCreateTypeCoin(Request $request)
  {
    $v = Validator::make($request->all(), [
      'id_coin' => 'required|max:255|min:2',
      'name' => 'required|max:255|min:2',
      'symbol' => 'required|max:255|min:2',
      'file' => 'required',
      'rate_buy' => 'required',
      'rate_sell' => 'required',
      'min_buy' => 'required',
      'max_buy' => 'required',
      'min_sell' => 'required',
      'max_sell' => 'required'
    ]);
    if ($v->fails()){
      return redirect()->back();
    } else {
      $imageName = time().'.'.request()->file->getClientOriginalExtension();
      $tag = $request->tag != '' ? $request->tag : null;
      if (request()->file->move(public_path('img'), $imageName)) {
        $state = $request->state == 'on' ? 1 : 0;
        DB::table('tb_coin')->insert(
          ['id_coin' => $request->id_coin, 'img' => '/img/'. $imageName, 'name' => $request->name, 'symbol' => $request->symbol, 'state' => $state, 'rate_buy' => $request->rate_buy, 'rate_sell' => $request->rate_sell, 'min_buy' => $request->min_buy, 'max_buy' => $request->max_buy, 'min_sell' => $request->min_sell, 'max_sell' => $request->max_sell, 'address_recive' => $request->address_recive != '' ? $request->address_recive : null, 'tag' => $tag]
        );
        return redirect('adbank/type');
      } else {
        return redirect()->back();
      }
    }
  }
  public function editTypeCoin($id = 0)
  {
    $data = DB::table('tb_coin')->where('id', $id)->first();
    if (empty($data)) {
      return redirect()->back();
    }
    return view('admin.edit_type_coin', ['data' => $data]);
  }
  public function postEditTypeCoin($id = 0, Request $request)
  {
    $v = Validator::make($request->all(), [
      'id_coin' => 'required|max:255|min:2',
      'name' => 'required|max:255|min:2',
      'symbol' => 'required|max:255|min:2',
      'rate_buy' => 'required',
      'rate_sell' => 'required',
      'min_buy' => 'required',
      'max_buy' => 'required',
      'min_sell' => 'required',
      'max_sell' => 'required'
    ]);
    if ($v->fails()){
      return redirect()->back();
    }
    $state = $request->state == 'on' ? 1 : 0;
    $tag = $request->tag != '' ? $request->tag : null;
    $dataPost = ['id_coin' => $request->id_coin, 'name' => $request->name, 'symbol' => $request->symbol, 'state' => $state, 'rate_buy' => $request->rate_buy, 'rate_sell' => $request->rate_sell, 'min_buy' => $request->min_buy, 'max_buy' => $request->max_buy, 'min_sell' => $request->min_sell, 'max_sell' => $request->max_sell, 'address_recive' => $request->address_recive != '' ? $request->address_recive : null, 'tag' => $tag];
    if ($request->hasFile('file')) {
      $imageName = time().'.'.request()->file->getClientOriginalExtension();
      if (request()->file->move(public_path('img'), $imageName)) {
        $dataPost['img'] = '/img/'. $imageName;
        DB::table('tb_coin')->where('id', $id)->update($dataPost);
      } else {
        return redirect()->back();
      }
    } else {
      DB::table('tb_coin')->where('id', $id)->update($dataPost);
    }
    return redirect('adbank/type');
  }
  public function postDeleteTypeCoin($id = 0)
  {
    DB::table('tb_coin')->where('id', $id)->delete();
    return redirect('adbank/type');
  }
  public function config()
  {
    $data = DB::table('tb_config')->first();
    return view('admin.config', ['data' => $data]);
  }
  public function postConfig(Request $request)
  {
    $v = Validator::make($request->all(), [
      'place' => 'required|max:255|min:2',
      'phone_1' => 'required|max:255|min:2',
      'phone_2' => 'required|max:255|min:2',
      'facebook' => 'required|max:255|min:2',
      'email' => 'required|max:255|min:2|email',
      'hour_start' => 'required|max:255|min:4',
      'hour_end' => 'required|max:255|min:4',
      'hour_start_a' => 'required|max:255|min:4',
      'hour_end_a' => 'required|max:255|min:4',
      'logo' => 'required|max:255|min:4',
      'banner_1' => 'required|max:255|min:4',
      'banner_2' => 'required|max:255|min:4',
      'time_out' => 'required'
    ]);
    if ($v->fails()){
      return redirect()->back();
    }
    $bank_name = $request->bank_name != '' ? $request->bank_name : null;
    $bank_place = $request->bank_place != '' ? $request->bank_place : null;
    $bank_name_user = $request->bank_name_user != '' ? $request->bank_name_user : null;
    $bank_number = $request->bank_number != '' ? $request->bank_number : null;
    $dieukhoan = $request->dieukhoan != '' ? $request->dieukhoan : null;

    $dataPost = ['address' => $request->place, 'phone_1' => $request->phone_1, 'phone_2' => $request->phone_2, 'facebook' => $request->facebook, 'email' => $request->email, 'notification' => $request->notification != '' ? $request->notification : null, 'hour_start' => $request->hour_start, 'hour_end' => $request->hour_end, 'logo' => $request->logo, 'banner_1' => $request->banner_1, 'banner_2' => $request->banner_2, 'bank_name' => $bank_name, 'bank_place' => $bank_place, 'bank_number' => $bank_number, 'bank_name_user' => $bank_name_user, 'timeout_load' => $request->time_out, 'content_dieukhoan' => $dieukhoan, 'hour_start_1' => $request->hour_start_a, 'hour_end_1' => $request->hour_end_a];
    DB::table('tb_config')->update($dataPost);
    return redirect('/adbank/config');
  }
  public function listTransactionBuy()
  {
    $data = DB::table('tb_sell_buy')
                  ->join('tb_user', 'tb_user.id', '=', 'tb_sell_buy.account_id')
                  ->join('tb_coin', 'tb_coin.id_coin', '=', 'tb_sell_buy.type_coin')
                  ->where('tb_sell_buy.type_action', 1)
                  ->orderBy('tb_sell_buy.id', 'desc')
                  ->select('tb_sell_buy.*', 'tb_user.email', 'tb_user.phone', 'tb_coin.name')
                  ->paginate(20);
    $index = 0;
    foreach ($data as $p) {
      $data[$index]->timeEnd = '';
      $data[$index]->timeEnd = date("Y/m/d H:i:s", strtotime($p->time) + 60 * 5);
      $index ++;
    }
    return view('admin.list_transaction_buy', ['data' => $data]);
  }
  public function editTransactionBuy($id = 0)
  {
    $data = DB::table('tb_sell_buy')->where('id', $id)->first();
    if(empty($data))  return redirect()->back();
    return view('admin.edit_transaction_buy', ['data' => $data]);
  }
  public function postTransactionBuy($id = 0, Request $request)
  {
    $v = Validator::make($request->all(), [
      'money' => 'required|min:0|alpha_num',
      'state_pay' => 'required|numeric|min:0|max:1',
      'state_transaction' => 'required|numeric|min:0|max:2',
    ]);
    if ($v->fails()){
      return redirect()->back();
    }
    DB::table('tb_sell_buy')->where('id', $id)->update(['money' => $request->money, 'is_pay' => $request->state_pay, 'state' => $request->state_transaction]);
    return redirect('/adbank/transaction-buy');
  }
  public function deleteTransactionBuy($id = 0)
  {
    $data = DB::table('tb_sell_buy')->where('id', $id)->delete();
     return redirect('/adbank/transaction-buy');
  }
  public function listTransactionSell()
  {
    $data = DB::table('tb_sell_buy')
                  ->join('tb_user', 'tb_user.id', '=', 'tb_sell_buy.account_id')
                  ->join('tb_coin', 'tb_coin.id_coin', '=', 'tb_sell_buy.type_coin')
                  ->where('tb_sell_buy.type_action', 0)
                  ->orderBy('tb_sell_buy.id', 'desc')
                  ->select('tb_sell_buy.*', 'tb_user.email', 'tb_user.phone', 'tb_coin.name')
                  ->paginate(20);
    $index = 0;
    foreach ($data as $p) {
      $data[$index]->timeEnd = '';
      $data[$index]->timeEnd = date("Y/m/d H:i:s", strtotime($p->time) + 60 * 180);
      $index ++;
    }
    return view('admin.list_transaction_sell', ['data' => $data]);
  }
  public function editTransactionSell($id = 0)
  {
    $data = DB::table('tb_sell_buy')->where('id', $id)->first();
    if(empty($data))  return redirect()->back();
    return view('admin.edit_transaction_sell', ['data' => $data]);
  }
  public function postTransactionSell($id = 0, Request $request)
  {
    $v = Validator::make($request->all(), [
      'money' => 'required|min:0|alpha_num',
      'state_pay' => 'required|numeric|min:0|max:1',
      'state_transaction' => 'required|numeric|min:0|max:2',
    ]);
    if ($v->fails()){
      return redirect()->back();
    }
    DB::table('tb_sell_buy')->where('id', $id)->update(['money' => $request->money, 'is_pay' => $request->state_pay, 'state' => $request->state_transaction]);
    return redirect('/adbank/transaction-sell');
  }
  public function deleteTransactionSell($id = 0)
  {
    $data = DB::table('tb_sell_buy')->where('id', $id)->delete();
    return redirect('/adbank/transaction-sell');
  }
}