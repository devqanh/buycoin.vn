<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login-admin', 'AdminController@login');
Route::post('/post-login-admin', 'AdminController@postLogin');
Route::group(['prefix' => '/adbank', 'middleware' => 'loginAdmin'], function () {
    Route::get('/', function (){
        return redirect('/adbank/users');
    });
    Route::get('/logout', 'AdminController@logout');
    Route::get('/change-password', 'AdminController@changePassword');
    Route::post('/post-change-password', 'AdminController@postChangePass');

    // user
    Route::get('users', 'AdminController@listUsers');
    Route::get('lock-user/{id}', 'AdminController@lockUser')->where('id', '[0-9]+');
    Route::get('unlock-user/{id}', 'AdminController@unlockUser')->where('id', '[0-9]+');
    Route::get('search-users', 'AdminController@listUsers');
    // news
    Route::get('create-new', 'AdminController@createNew');
    Route::post('/post-create-new', 'AdminController@postCreateNew');
    Route::get('/edit-new/{id}', 'AdminController@editNew')->where('id', '[0-9]+');
    Route::post('/post-edit-new/{id}', 'AdminController@postEditNew')->where('id', '[0-9]+');
    Route::get('/news', 'AdminController@listNew');
    Route::get('/delete-new/{id}', 'AdminController@deleteNew')->where('id', '[0-9]+');
    // type
    Route::get('/type', 'AdminController@listType');
    Route::get('/edit-type/{id}', 'AdminController@editType');
    Route::post('/post-edit-type', 'AdminController@postEditType');
    Route::get('/type/create', 'AdminController@createTypeCoin');
    Route::post('/post-create-type-coin', 'AdminController@postCreateTypeCoin');
    Route::get('/edit-type-coin/{id}', 'AdminController@editTypeCoin')->where('id', '[0-9]+');
    Route::get('/delete-type-coin/{id}', 'AdminController@postDeleteTypeCoin')->where('id', '[0-9]+');
    // help
    Route::get('/create-help', 'AdminController@createHelp');
    Route::post('/post-create-help', 'AdminController@postCreateHelp');
    Route::get('/help', 'AdminController@listHelp');
    Route::get('/edit-help/{id}', 'AdminController@editHelp')->where('id', '[0-9]+');
    Route::post('/post-edit-help/{id}', 'AdminController@postEditHelp')->where('id', '[0-9]+');
    Route::get('/delete-help/{id}', 'AdminController@deleteHelp')->where('id', '[0-9]+');
    // report
    Route::get('/report', 'AdminController@listReport');
    Route::get('/create-report', 'AdminController@createReport');
    Route::post('/post-create-report', 'AdminController@postCreateReport');
    Route::get('/edit-report/{id}', 'AdminController@editReport')->where('id', '[0-9]+');
    Route::post('/post-edit-report/{id}', 'AdminController@postEditReport')->where('id', '[0-9]+');
    Route::get('/delete-report/{id}', 'AdminController@deleteReport')->where('id', '[0-9]+');
    Route::post('/post-edit-type-coin/{id}', 'AdminController@postEditTypeCoin')->where('id', '[0-9]+');
    // config
    Route::get('/config', 'AdminController@config');
    Route::post('/post-config', 'AdminController@postConfig');
    // transaction
    Route::get('/transaction-buy', 'AdminController@listTransactionBuy');
    Route::get('/transaction-buy/edit/{id}', 'AdminController@editTransactionBuy')->where('id', '[0-9]+');
    Route::post('/post-edit-transaction-buy/{id}', 'AdminController@postTransactionBuy')->where('id', '[0-9]+');
    Route::get('/transaction-buy/delete/{id}', 'AdminController@deleteTransactionBuy')->where('id', '[0-9]+');
    Route::get('/transaction-sell', 'AdminController@listTransactionSell');
    Route::get('/transaction-sell/edit/{id}', 'AdminController@editTransactionSell')->where('id', '[0-9]+');
    Route::post('/post-edit-transaction-sell/{id}', 'AdminController@postTransactionSell')->where('id', '[0-9]+');
    Route::get('/transaction-sell/delete/{id}', 'AdminController@deleteTransactionSell')->where('id', '[0-9]+'); 

});

// home
Route::post('/post-register-user', 'HomeController@postRegister');
Route::get('/login', 'HomeController@login')->name('login');
Route::post('/post-login', 'HomeController@postLogin');

Route::get('/dieu-khoan', 'HomeController@dieukhoan');
Route::get('/', 'HomeController@home');
Route::get('/sell', 'HomeController@sellBTC');
Route::get('/change-password', 'HomeController@changePassword');
Route::get('/register', 'HomeController@register');
Route::get('/find', 'HomeController@searchTransaction');
Route::get('/news', 'HomeController@new');
Route::get('/news/{slug}', 'HomeController@newDetail');
Route::get('/guide/{slug}', 'HomeController@newGuide');
Route::get('/guide', 'HomeController@guide');
Route::get('/get-rate-buy-sell', 'HomeController@getListCoinShow');
Route::get('/reviews', 'HomeController@reviews');
Route::get('/get-account-name-bank/{id}', 'HomeController@getNameAcountBank')->where('id', '[0-9]+');
Route::post('/post-forget-password', 'HomeController@forgetPass');
Route::get('/quen-mat-khau', 'HomeController@regetPass');
Route::get('/reset-pass/{email}', 'HomeController@resetPassword');
Route::post('/post-reset-pass', 'HomeController@postResetPassword');

Route::middleware(['auth'])->group(function () {   
    Route::get('/account', 'HomeController@account');
    Route::post('/post-edit-profile', 'HomeController@editProfile');
    Route::post('/post-change-pass', 'HomeController@postChangePass');
    Route::get('/logout', 'HomeController@Logout');
    Route::post('/register-sell', 'HomeController@registerSell');
    Route::get('/register-sell', function ()
    {
        return redirect('/');
    });
    Route::post('/register-buy', 'HomeController@registerBuy');
    Route::get('/history', 'HomeController@historyTransaction');
    Route::get('/find-transaction', 'HomeController@postSearch');
    Route::get('/detail-order-buy/{id}', 'HomeController@detailOrderBuy')->where('id', '[0-9]+');
    Route::get('/cancel-transaction-buy/{id}', 'HomeController@cancelTranSactionBuy')->where('id', '[0-9]+');
    Route::get('/detail-order-sell/{id}', 'HomeController@detailOrderSell')->where('id', '[0-9]+');
    Route::get('/cancel-transaction-sell/{id}', 'HomeController@cancelTranSactionSell')->where('id', '[0-9]+');
});