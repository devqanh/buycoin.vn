var h = $('#time-count-down-d').attr('h')
	var m = $('#time-count-down-d').attr('m')
	var s = $('#time-count-down-d').attr('s')
	function start(){
	  if (s == -1) {
	  	s = 59
	  	m -= 1
	  }
	  if (m == -1) {
	  	m = 59
	  	h -= 1
	  }  
	  if (h == -1) {
	  	clearTimeout(timeout);
	  	return;
	  }
	  $('#cd-h').html('0' + h + ' : ')
	  var tempLabelM = m > 9 ? m + ' : ' : '0' + m + ' : '
	  $('#cd-m').html(tempLabelM)
	  var tempLabelS = s > 9 ? s : '0' + s
	  $('#cd-s').html(tempLabelS)
    timeout = setTimeout(function(){
      s--;
      start();
    }, 1000);
	}
	if (h != 0 || m != 0 || s != 0) {
		start()
	}