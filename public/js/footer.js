$('.dropdown-button').dropdown({
  inDuration: 300,
  outDuration: 225,
  constrainWidth: false, // Does not change width of dropdown to that of the activator
  hover: false, // Activate on hover
  gutter: 0, // Spacing from edge
  belowOrigin: true, // Displays dropdown below the button
  alignment: 'left', // Displays dropdown with edge aligned to the left of button
  stopPropagation: false // Stops event propagation
}
);
$('.button-collapse').sideNav({
  menuWidth: 230, // Default is 300
  edge: 'left', // Choose the horizontal origin
  closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
  draggable: true, // Choose whether you can drag to open on touch screens,
  onOpen: function(el) { /* Do Stuff */ }, // A function to be called when sideNav is opened
  onClose: function(el) { /* Do Stuff */ }, // A function to be called when sideNav is closed
});
// const activeDatePicker = () => {
// 	$('.datepicker').pickadate({
// 	  selectMonths: true, // Creates a dropdown to control month
// 	  selectYears: 100, // Creates a dropdown of 15 years to control year,
// 	  today: 'Today',
// 	  clear: 'Clear',
// 	  close: 'Ok',
// 	  closeOnSelect: true // Close upon selecting a date,
// 	});
// }
// activeDatePicker()
$('.modal').modal({
  dismissible: false, // Modal can be dismissed by clicking outside of the modal
  opacity: .5, // Opacity of modal background
  inDuration: 0, // Transition in duration
  outDuration: 0, // Transition out duration
  startingTop: '10%', // Starting top style attribute
  endingTop: '5%', // Ending top style attribute
});
// $('#pickerTimeStart').datepicker({
//   'format': 'yyyy-mm-dd',
//   'autoclose': true
// });
const activeDatePicker = () => {
	const listInputDate = $('.date-picker')
	for (var i = 0; i < listInputDate.length; i++) {
		$(listInputDate[i]).datepicker({
		  'format': 'yyyy-mm-dd',
		  'autoclose': true
		});
	}
}
activeDatePicker()
controlNotificationTab = (event) => {
	event.stopPropagation();
	if ($('.content-notification').css('display') == 'none') {
		$('.content-notification').show()
		if (haveComment) {
			$.get('/active-read-comment', (r) => {
				if (r.state) {
					$('.notification small').html(0).hide()
				}
			})
		}
	} else {
		$('.content-notification').hide()
	}
}
const activeShowConfirmDelete = () => {
	const listDelete = $('.delete-element')
	for (let i = 0; i < listDelete.length; i++) {
		$(listDelete[i]).click(function () {
			const url = $(this).attr('href')
			mbox.custom({
				message: '<div class="css-text-box">' + msgDelete + '</div>',
				buttons: [
	        {
	          label: msgCancel,
	          color: 'red darken-2'
	        },
	        {
	          label: msgOkCancel,
	          color: 'teal darken-2',
	          callback: function() {
	            window.location.href = url
	          }
	        }
	    	]
			})
			return false
		})
	}
}
activeShowConfirmDelete()
$(document).click(function() {
	$('.hide-when-document-click').hide()
})
$('.hide-when-document-click').click(function(event) {
	event.stopPropagation()
	$(this).show()
})
let haveComment = false
const getNofification = () => {
	$.get('/get-notification', (r) => {
		if (r.state) {
			let s = ''
			if (r.content.length == 0) {
				$('.content-notification').html(`<a style="color: red"> Không có thông báo nào </a>`)
				$('.notification small').hide()
				return 
			}
			if (r.content.filter(e => !e.is_view).length == 0) {
				$('.notification small').hide()
			} else {
				haveComment = true
				$('.notification small').html(r.content.filter(e => !e.is_view).length).show()
			}
			r.content.map(e => {
				if (e.type == 0) {
					s += `<a href="${e.link}">
        				<img src="${e.account_from_avatar}">
        				<div>${e.account_from_name} vừa bình luận trong đăng ký nguồn lực (${e.user_name} - ${moment(e.date).format('DD/MM/YYYY')})</div>
        			</a>`
				}
				if (e.type == 1) {
					s += `<a href="${e.link}">
        				<img src="${e.account_from_avatar}">
        				<div>${e.account_from_name} vừa bình luận trong chấm công (${e.project_code} - ${moment(e.date).format('DD/MM/YYYY')})</div>
        			</a>`
				}
				if (e.type == 2) {
					s += `<a href="${e.link}">
        				<img src="${e.account_from_avatar}">
        				<div>${e.account_from_name} vừa yêu cầu phê duyệt đăng ký nguồn lực</div>
        			</a>`
				}
				if (e.type == 3) {
					s += `<a href="${e.link}">
        				<img src="${e.account_from_avatar}">
        				<div>${e.account_from_name} vừa yêu cầu phê duyệt chấm công</div>
        			</a>`
				}
			})
			$('.content-notification').html(s)
		} else {
			$('.notification small').hide()
			$('.content-notification').html(`<a style="color: red"> Không có thông báo nào </a>`)
		}
	})
}
getNofification();
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
  	mbox.custom({
			message: '<input id="name-file-export-excel" placeholder="Nhập tên file"> </input>',
			buttons: [
		    {
		      label: msgCancel,
		      color: 'red darken-2'
		    },
		    {
		      label: msgOkCancel,
		      color: 'teal darken-2',
		      callback: function() {
		      	const fileName = $('#name-file-export-excel').val().trim()
		        if (fileName == '') {
		        	Materialize.toast('File name is required', 1000)
		        	return;
		        }
			      if (!table.nodeType) table = document.getElementById(table)
		        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
		        // window.location.href = uri + base64(format(template, ctx))
		        var link = document.createElement("a");
				    link.download = fileName + ".xls";
				    link.href = uri + base64(format(template, ctx));
				    link.click();
				    mbox.close()
				  }
				}
			]
		})
	}
})()