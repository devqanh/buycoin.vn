var validateAccount = false
var validateURL = false
function checkNumber() {
	if (!checkTimeTransaction($('#time-work-day').attr('hour_start'), $('#time-work-day').attr('hour_end')) && !checkTimeTransaction($('#time-work-day').attr('hour_start_1'), $('#time-work-day').attr('hour_end_1'))) {
		alert('Ngoài thời gian làm việc, vui lòng quay lại sau')
		return false
	}
	if (!validateAccount) {
		$('.error-msg-account').html('Vui lòng nhập số tài khoản vietcombank')
		return false
	}
	if (!validateURL) {
		$('.error-msg-url').html(`Vui lòng nhập địa chỉ ví`)
		return false
	}
	var numberCoin = parseFloat($('#number-coin').val())
	var min = parseFloat($('#type-coin option:selected').attr('min'))
	var max = parseFloat($('#type-coin option:selected').attr('max'))
	if (numberCoin < min) {
		alert('Số lượng bán tối thiểu là: ' + min)
		return false
	}
	if (numberCoin > max) {
		alert('Số lượng bán tối đa là: ' + max)
		return false
	}
	return true
}
function calMoney() {
	if ($('#number-coin').val() == '') {
		$('#ammount_cal').val('')
		return ;
	}
	var numberCoin = parseFloat($('#number-coin').val())
	var ammount = parseFloat($('#type-coin option:selected').attr('ammount'))
	var valC = numberCoin * ammount
	$('#ammount_cal').val(numberFormat(valC))
}
function getNameAccount(pastedData) {
	var ev = $('#id-account-bank')
	var dataEnter = pastedData ? pastedData : ev.val()
	if (dataEnter.length != 13) {
		ev.next().hide()
		ev.next().next().html('Vui lòng nhập số tài khoản vietcombank')
		validateAccount = false
	} else {
		ev.next().show()
		ev.next().next().html('')
		validateAccount = true
	}
}
function checkEnterTag(ev) {
	if ($(ev).prop('checked') == true) {
		$(ev).parent().next().find('input').removeAttr('disabled')
		$(ev).parent().next().find('input').attr('required', true)
	} else {
		$(ev).parent().next().find('input').removeAttr('required')
		$(ev).parent().next().find('input').attr('disabled', true)
	}
}
function checkUrlS(pastedData = false) {
	var l = parseInt($('#type-coin option:selected').attr('lu'), 10)
	var ev = $('#url-coin')
	var dataEnter = pastedData ? pastedData : ev.val()
	if (dataEnter.length != l) {
		ev.next().hide()
		ev.next().next().html(`Vui lòng nhập địa chỉ ví`)
		validateURL = false
	} else {
		ev.next().show()
		ev.next().next().html(``)
		validateURL = true
	}
}
$("#url-coin").bind("paste", function(e){
    var pastedData = e.originalEvent.clipboardData.getData('text');
    checkUrlS(pastedData)
});
$("#id-account-bank").bind("paste", function(e){
    var pastedData = e.originalEvent.clipboardData.getData('text');
    getNameAccount(pastedData)
});
function checkInMobile() {
	setInterval(function(){
		checkUrlS()
	}, 3);
}
function checkInMobileA() {
	setInterval(function(){
		getNameAccount()
	}, 3);
}