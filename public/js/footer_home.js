$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
function confirmCancel() {
	var check = confirm('Bạn có chắc chắn hủy giao dịch ?')
	if (check) {
		return true;
	} else {
		return false;
	}
}
function redirectRegister() {
	if ($('#approve-dk').prop('checked') == true) {
		window.location.href = '/register';
	} else {
		alert('Vui lòng tích vào nút đồng ý điều khoản')
	}
}
function copyToClipboard(ev) {
	$(ev).prev().select();
	document.execCommand("copy");
	var temp = '<p id="da-sao-chep"> Đã sao chép </p>'
	$('body').append(temp)
	setTimeout(function(){ $('#da-sao-chep').remove()  }, 2000);
}