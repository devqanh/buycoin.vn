﻿$('.carousel').carousel({
	interval: 5000
})
	window.onload=function(){setTimeout(function(){$('#waiting').fadeOut('fast')},300)}
	$('[type="submit"]').click(function(){
		if($("[name='send']").val() != '' && $('[name="account"]').val() != '' && $('[name="account_name"]').val() != ''){
			setTimeout($('#waiting').show(), 2000);
		}
	});

	function setPayment() {
		Recived = $("[name='bankrecived']").val();
		$('[name="account"]').val('');
		$('[name="account_name"]').val('');
		$('.account_load').html('');
		$('[name="account"]').removeAttr('disabled');
		$('[name="account"]').attr('placeholder','Số tài khoản nhận tiền của bạn.');
		$('[name="account"]').focus();
		if(Recived == 'btce'){
			$("#recivedaccount").css({"display" : "none"});
		} else {
			$("#recivedaccount").css({"display" : "block"});
		}
	}
	
	function remove_char_invisible(str) {
		str2 = "";
		for (i=0; i<str.length; i++) {
			cca = str.charCodeAt(i);
			if (cca > 31 && cca <127)
				str2+= str[i];
		}
		return str2;
	}
	
	function checkaccount(accountnumber, bankcode){
		$.ajax({ type: "GET", url: Base_Url + '/getname/',	data: 'account=' + accountnumber + '&bank_code=' + bankcode.toLowerCase(), 
			success: function(response){ 
				vuta = $.parseJSON(remove_char_invisible(response));
				if(vuta.status){
					if(jsonRates.action == 'sell') { 
						$(".account_load").html(vuta.account_name);
					} else {
						if(jsonRates.bank == 'pm' || jsonRates.bank == 'wmz') {
							$(".account_load").html(vuta.account_name);
						} else {
							$(".account_load").html('<i class="fa fa-check" style="font-size: 15pt;color:#37aa57;"></i> ');
						}
					}
					$("#bank_error").html('');
					if($("[name='send']").val() != '' && $("[name='account']").val() != '') {
						$('.submit').removeAttr('disabled');
					}
				} else {
					$(".account_load").html('<i class="fa fa-times" style="font-size: 15pt;color:#d9534f;"></i>');
					if(jsonRates.action == 'sell') {
						$("#bank_error").html('Không tìm được tên tài khoản, vui lòng kiểm tra lại số tài khoản của bạn.');
					} else {
						$("#bank_error").html('Địa chỉ ví bạn vừa nhập chưa đúng, vui lòng kiểm tra lại.');
					}
					$('.submit').attr('disabled','disabled');
				}
			}, 
			error: function(XMLHttpRequest, textStatus, errorThrown) { } 
		});
	}	
	
	$(document).ready(function(){
		$("[name=account]").focusout(function(){ 
			if($("[name=account]").val() != '') {
				$("[name=account_name]").val("Đang tìm tên tài khoản...");
				$(".account_load").html('<i class="fa fa-circle-o-notch fa-spin fa-fw" style="font-size: 15pt;color:#999"></i>');
				checkaccount($("[name=account]").val(), $("[name='bankrecived']").val());
			}
		}); 
		
		$('#rate').html(number_format(jsonRates.rate, ".", 0, ",") + ' VNĐ'); // load tỷ giá áp dụng

		$('#pm_form_action').submit();
		$('#wmz_form_action').submit();
		rate_ajax_load();
		balance_ajax_load();
    });
	
	$("[name='send']").keyup(function(){
		$('select').removeAttr('disabled');
		$('[name="recived"]').focus();
		$('[name="send"]').focus();
		if (this.value < parseFloat(jsonRates.min) || isNaN(this.value)){
			$('#num_error').html('<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Số lượng tối thiểu: ' + jsonRates.min);
			$("[name=recived]").val(0);
			$("[name='send']").focus();
			$(".formRed").removeClass("formBlue");
			$(".labelRed").removeClass("labelBlue");
			$('.submit').attr('disabled','disabled');
			return false;
		} else { 
			$('#num_error').html('');
			$(".formRed").addClass("formBlue");
			$(".labelRed").addClass("labelBlue");
			$('#rate_apply').html('<i class="fa fa-check" aria-hidden="true"></i> Tỷ giá được áp dụng: ' + number_format(jsonRates.rate, ".", 0, ",") + ' VNĐ/' + jsonRates.bank.toUpperCase());
		}
		money = (this.value * jsonRates.rate);
		$("[name=recived]").val(number_format(money, ".", 0, ",") + ' VNĐ');
	});
	
	$(":input").bind("keyup change", function(e) {
		send = $("[name='send']").val();
		recived = $("[name='recived']").val();
		bankrecived = $("[name='bankrecived']").val();
		account = $("[name='account']").val();
		account_name = $("[name='account_name']").val();
		if(send && recived && bankrecived && account && account_name) {
			$('.submit').removeAttr('disabled');
		}
	});
	
	function keyNumber(e){
		var keyword=null;
			if(window.event){
				keyword=window.event.keyCode;
			} else {
				keyword=e.which; //NON IE;
			}
			
			if(keyword < 48 || keyword > 57){
				if(keyword == 48 || keyword == 127) {
					return ;
				}
				return false;
			}
		}
	
	function number_format(number, decimals, dec_point, thousands_sep){
		var n = number, prec = decimals;
		
		var toFixedFix = function (n,prec) {
			var k = Math.pow(10,prec);
			return (Math.round(n*k)/k).toString();
		};
		
		n = !isFinite(+n) ? 0 : +n;
		prec = !isFinite(+prec) ? 0 : Math.abs(prec);
		var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
		var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;
		
		var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;
		
		var abs = toFixedFix(Math.abs(n), prec);
		var _, i;
		
		if (abs >= 1000) {
			_ = abs.split(/\D/);
			i = _[0].length % 3 || 3;
		
			_[0] = s.slice(0,i + (n < 0)) +
				  _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
			s = _.join(dec);
		} else {
			s = s.replace('.', dec);
		}
		
		var decPos = s.indexOf(dec);
		if (prec >= 1 && decPos !== -1 && (s.length-decPos-1) < prec) {
			s += new Array(prec-(s.length-decPos-1)).join(0)+'0';
		}
		else if (prec >= 1 && decPos === -1) {
			s += dec+new Array(prec).join(0)+'0';
		}
		return s;
	}
	
	function timer(timedate, typedate){
		a = new Date();
		b = new Date(timedate*1000);
		baygio = a.getTime();
		strreturn = "";
		switch(typedate){
			default:
				if ((remain=baygio-timedate)>=86400)
					strreturn = b.getDate() + "-" + b.getMonth() + "-" + b.getFullYear();
				else
				{
					if (remain>=3600)
						strreturn = parseInt(remain/3600) + " giờ trước";
					else if (remain>=60)
						strreturn = parseInt(remain/60) + " phút trước";
					else
						strreturn = remain > 0 ? remain + " giây trước" : "Vừa mới xong";
				}
				break;  
		};
		return strreturn;
	}
	
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
		
	function rate_ajax_load(){
		var timeOutId = 0;
		var ajaxFn = function () {
				$('.rate_loading').removeClass('eRed eGreen');
				$.ajax({
					url: Base_Url + '/ajax_rate/',
					timeout: 0,
					success: function (response) {
						data = $.parseJSON(response);
						for(var key in data) { 
							if(data.hasOwnProperty(key)) {
								if(key != 'last_update') { 
									var sellKey = $("#" + key + "_rate_sell").text().replace(/[^0-9]/g, '');
									var buyKey = $("#" + key + "_rate_buy").text().replace(/[^0-9]/g, '');
									if(data[key]['sell']['amount'] < sellKey) {
										$("#" + key + "_rate_sell").addClass('eRed');
									} else if(data[key]['sell']['amount'] > sellKey) { 
										$("#" + key + "_rate_sell").addClass('eGreen');
									}
									if(data[key]['buy']['amount'] < buyKey) {
										$("#" + key + "_rate_buy").addClass('eRed');
									} else if(data[key]['buy']['amount'] > buyKey) { 
										$("#" + key + "_rate_buy").addClass('eGreen');
									}
									$("#" + key + "_rate_sell").html(number_format(data[key]['sell']['amount'], ".", 0, ",") + ' ₫');
									$("#" + key + "_rate_buy").html(number_format(data[key]['buy']['amount'], ".", 0, ",") + ' ₫');
								}
							}
						}
						for(var key in data['last_update']) {
							if(data['last_update'].hasOwnProperty(key)) {
							 	$("#" + key).html(data['last_update'][key]);
							}
						}
						timeOutId = setTimeout(ajaxFn, 10000);
						$.ajaxSetup({ cache: false });
					}
				});
		}
		timeOutId = setTimeout(ajaxFn, 10000);	
	}
		
	function balance_ajax_load(){
		var timeOutId = 0;
		var ajaxFn = function () {
				$.ajax({
					url: Base_Url + '/ajax_balance/',
					timeout: 0,
					success: function (response) {
						data = $.parseJSON(response);
						for(var key in data) {
							if(data.hasOwnProperty(key)) {
							 	$("#" + key + "_balance").html(data[key]['balance']);
							}
						}
						timeOutId = setTimeout(ajaxFn, 10000);
						$.ajaxSetup({ cache: false });
					}
				});
		}
		timeOutId = setTimeout(ajaxFn, 10000);	
	}
		
	function check_payment(orderid){
		var timeOutId = 0;
		var orderid = orderid;
		var ajaxFn = function () {
				$.ajax({
					url: Base_Url + '/check_payment/' + orderid,
					timeout: 0,
					success: function (response) {
						data = $.parseJSON(response);
						if (data.status == 0) {
							$("#payment_waiting").html(data.html);
							timeOutId = setTimeout(ajaxFn, 5000);
						}
						if (data.status == 5) {
							// $('.payment_send_info').html('');
							$("#payment_waiting").html(data.html);
							timeOutId = setTimeout(ajaxFn, 5000);
						}
						if (data.status == 1 || data.status == 3 || data.status == 4) {
							// $('.payment_send_info').html('');
							$("#transaction_rate").html(data.rate);
							$("#transaction_fist_rate").html(data.old.rate);
							if(data.action == 'sell') {
								$("#transaction_send").html(data.send);
								$("#transaction_recived").html(number_format(data.recived));
							} else {
								$("#transaction_send").html(number_format(data.recived));
								$("#transaction_recived").html(data.send);
							}
							$("#payment_waiting").html(data.html);
							timeOutId = setTimeout(ajaxFn, 5000);
						}
						if (data.status == 2) {
							$("#payment_waiting").html(data.html);
							window.location.reload();
						}
					}
				});
		}
		timeOutId = setTimeout(ajaxFn, 5000);	
	}
	
	$(function() {
		var clipboard = new Clipboard('.text-copy');
		clipboard.on('success', function(e) {
			alert('Đã sao chép: ' + e.text);
			e.clearSelection();
		});
	});
