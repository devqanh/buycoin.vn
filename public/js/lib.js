const timeToast = 3000
const activeTolltip = () => {
	$('.tooltipped').tooltip({
		delay: 0,
		position: 'top',
	});
}
// loadding
const loadingSend = (ob, state, oldName) => {
	if (state) {
		ob.attr('disabled', 'disabled')
		ob.html(`<i class="fa fa-spinner fa-spin fa-fw"></i>`)
	} else {
		ob.removeAttr('disabled')
		ob.html(oldName)
	}
}
const numberFormat = (number = '0', decimalNumber = 0) => {
	let decimal = '.'
	const thousand = ','
	const dataSeprate = String(number).split('.')
	const str = dataSeprate[0]
	if (str.length < 3) {
		return str
	}
	let temp = str.split("")
	let result = ''
	let surplus = 0
	if (temp.length % 3 === 0) surplus = 1
	if (temp.length % 3 === 1) surplus = 0
	if (temp.length % 3 === 2) surplus = 2
	temp.forEach((e, i) => {
		result += e
		if ((i + surplus) % 3 === 0 && i !== temp.length - 1) {
			result += thousand
		}
	})
	let labelThousand = ''
	if (decimalNumber !== 0) {
		if (dataSeprate.length === 0) {
			for (let i = 0; i < decimalNumber.length; i++) {
				labelThousand += '0'
			}
		} else {
			labelThousand = dataSeprate[1].substring(0, decimalNumber)
			if (decimalNumber > dataSeprate[1].length) {
				for (let i = 0; i < (decimalNumber - dataSeprate[1].length); i++) {
					labelThousand += '0'
				}
			}
		}
	}
	decimal = decimalNumber === 0 ? '' : decimal 
	return result + decimal + labelThousand
}
// convert string
const convertToSlug = (title) => {
	title = title ? title : ""
  let slug;
  slug = title.toLowerCase();
  slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
  slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
  slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
  slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
  slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
  slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
  slug = slug.replace(/đ/gi, 'd');
  slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
  slug = slug.replace(/ /gi, " - ");
  slug = slug.replace(/\-\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-/gi, '-');
  slug = slug.replace(/\-\-/gi, '-');
  slug = '@' + slug + '@';
  slug = slug.replace(/\@\-|\-\@|\@/gi, '');
  return slug
}
function checkTimeTransaction (timeStart, timeEnd) {
		var hourStart = parseInt(timeStart.split(':')[0], 10)
		var mStart = parseInt(timeStart.split(':')[1], 10)
		var hourEnd = parseInt(timeEnd.split(':')[0], 10)
		var mEnd = parseInt(timeEnd.split(':')[0], 10)
		var d = new Date()
	  var h = d.getHours()
	  var m = d.getMinutes()
		if (h < hourStart || (hourStart == h && m < mStart) || h > hourEnd || hourEnd == h && m > mEnd) {
			return false
		}
		return true
	}