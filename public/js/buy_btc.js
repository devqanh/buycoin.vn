var validateURL = false
function checkNumber() {
	if (!checkTimeTransaction($('#time-work-day').attr('hour_start'), $('#time-work-day').attr('hour_end')) && !checkTimeTransaction($('#time-work-day').attr('hour_start_1'), $('#time-work-day').attr('hour_end_1'))) {
		alert('Ngoài thời gian làm việc, vui lòng quay lại sau')
		return false
	}
	if (!validateURL) {
		var l = parseInt($('#type-coin option:selected').attr('lu'), 10)
		$('.error-msg-url').html(`Vui lòng nhập địa chỉ ví`)
		return false
	}
	var numberCoin = parseFloat($('#number-coin').val())
	var min = parseFloat($('#type-coin option:selected').attr('min'), 10)
	var max = parseFloat($('#type-coin option:selected').attr('max'), 10)
	if (numberCoin < min) {
		alert('Số lượng mua tối thiểu là: ' + min)
		return false
	}
	if (numberCoin > max) {
		alert('Số lượng mua tối đa là: ' + max)
		return false
	}
	return true
}
function calMoney() {
	var numberCoin = parseFloat($('#number-coin').val())
	var ammount = parseFloat($('#type-coin option:selected').attr('ammount'), 10)
	var valC = numberCoin * ammount
	$('#ammount_cal').val(numberFormat(valC))
	$('#ammount_cal_not_format').val(valC)
}
function checkEnterTag(ev) {
	if ($(ev).prop('checked') == true) {
		$(ev).parent().next().find('input').removeAttr('disabled')
		$(ev).parent().next().find('input').attr('required', true)
	} else {
		$(ev).parent().next().find('input').removeAttr('required')
		$(ev).parent().next().find('input').attr('disabled', true)
	}
}
function checkUrlS(pastedData = false) {
	var l = parseInt($('#type-coin option:selected').attr('lu'), 10)
	var ev = $('#url-coin')
	var dataEnter = pastedData ? pastedData : ev.val()
	if (dataEnter.length != l) {
		ev.next().hide()
		ev.next().next().html(`Vui lòng nhập địa chỉ ví`)
		validateURL = false
	} else {
		ev.next().show()
		ev.next().next().html(``)
		validateURL = true
	}
}
$("#url-coin").bind("paste", function(e){
    var pastedData = e.originalEvent.clipboardData.getData('text');
    checkUrlS(pastedData)
});
function checkInMobile() {
	setInterval(function(){
		checkUrlS()
	}, 4);
}
function checkInMobileA() {
	setInterval(function(){
		getNameAccount()
	}, 4);
}